﻿using Android.Content;
using CBrEaseMobile.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CBrEaseMobile.CustomControls.RoundEntry), typeof(RoundEntryRenderer))]
namespace CBrEaseMobile.Droid.CustomRenderers
{
    public class RoundEntryRenderer : EntryRenderer
    {
        public RoundEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Background = null;
            }
        }
    }
}