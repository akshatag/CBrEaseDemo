﻿using CBrEaseMobile.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(CBrEaseMobile.Droid.DependencyServices.FileHandling))]
namespace CBrEaseMobile.Droid.DependencyServices
{
    class FileHandling : IFileHandling
    {
        //private string LicenseFileName = "License.lic";
        private readonly static string CbreaseFolder = "CBrEase";
        private readonly static string LicenseStoragePath = "Android/License.lic";
        public bool SaveLicense(string text)
        {
            try
            {
                var rootPath = Android.OS.Environment.ExternalStorageDirectory.ToString();
                //var appPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var filePath = Path.Combine(rootPath, LicenseStoragePath);
                File.WriteAllText(filePath, text, Encoding.UTF8);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string GetLicense()
        {
            try
            {
                var rootPath = Android.OS.Environment.ExternalStorageDirectory.ToString();
                //var appPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var filePath = Path.Combine(rootPath, LicenseStoragePath);
                return File.ReadAllText(filePath);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        private readonly static string rootPath = Android.OS.Environment.ExternalStorageDirectory.ToString();
        private readonly static string folderPath = Path.Combine(rootPath, CbreaseFolder);

        public bool SaveProjectFile(byte[] stream, string fileName, string filePath)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                {
                    //new file
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);
                    filePath = Path.Combine(rootPath, CbreaseFolder, fileName);
                }
                var brease = new MemoryStream(stream);
                using (var streamReader = new StreamReader(brease))
                {
                    string content = streamReader.ReadToEnd();
                    File.WriteAllText(filePath, content);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Tuple<string, string, DateTime>> GetSavedFiles()
        {
            var listFile = new List<Tuple<string, string, DateTime>>();

            if (Directory.Exists(folderPath))
            {
                var files = Directory.GetFiles(folderPath);

                foreach (var filePath in files)
                {
                    var name = Path.GetFileName(filePath);
                    var time = File.GetCreationTimeUtc(filePath);

                    if (name.IndexOf(".cbz") > 0)
                        listFile.Add(Tuple.Create(name, filePath, time));
                }
            }
            return listFile;
        }

        public Stream GetFileStream(string path)
        {
            try
            {
                var file = File.ReadAllBytes(path);
                return new MemoryStream(file);
            }
            catch (Exception ex)
            {
                return Stream.Null;
            }
        }
    }
}