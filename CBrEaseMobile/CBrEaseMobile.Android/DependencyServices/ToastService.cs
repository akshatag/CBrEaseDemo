﻿using Android.App;
using Android.Widget;
using CBrEaseMobile.Services;

[assembly: Xamarin.Forms.Dependency(typeof(CBrEaseMobile.Droid.DependencyServices.ToastService))]
namespace CBrEaseMobile.Droid.DependencyServices
{
    public class ToastService : IToastService
    {
        public void ShowShortAlert(string message)
        {
            var toast = Toast.MakeText(Application.Context, message, ToastLength.Short);
            toast.Show();
        }

        public void ShowLongAlert(string message)
        {
            var toast = Toast.MakeText(Application.Context, message, ToastLength.Long);
            toast.Show();
        }
    }
}