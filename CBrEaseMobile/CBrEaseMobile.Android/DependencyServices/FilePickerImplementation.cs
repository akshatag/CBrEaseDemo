﻿using Android.App;
using Android.Content;
using CBrEaseMobile.FilePicker;
using CBrEaseMobile.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(CBrEaseMobile.Droid.DependencyServices.FilePickerImplementation))]
namespace CBrEaseMobile.Droid.DependencyServices
{
    public class FilePickerImplementation : IFilePicker
    {
        private readonly Context _context;
        private int _requestId;
        private TaskCompletionSource<FileData> _completionSource;

        public FilePickerImplementation()
        {
            _context = Application.Context;
        }

        public async Task<FileData> PickFile()
        {
            var media = await TakeMediaAsync("file/*", Intent.ActionGetContent);
            return media;
        }

        private Task<FileData> TakeMediaAsync(string type, string action)
        {
            var id = GetRequestId();

            var ntcs = new TaskCompletionSource<FileData>(id);

            if (Interlocked.CompareExchange(ref _completionSource, ntcs, null) != null)
            {
                //throw new InvalidOperationException("Only one operation can be active at a time");
                return null;
            }

            try
            {
                var pickerIntent = new Intent(_context, typeof(FilePickerActivity));
                pickerIntent.SetFlags(ActivityFlags.NewTask);
                //pickerIntent.SetType("application/cbz");
                _context.StartActivity(pickerIntent);

                EventHandler<FilePickerEventArgs> handler = null;
                EventHandler<EventArgs> cancelledHandler = null;

                handler = (s, e) =>
                {
                    var tcs = Interlocked.Exchange(ref _completionSource, null);
                    FilePickerActivity.FilePicked -= handler;
                    var stream = System.IO.File.OpenRead(e.FilePath);
                    var filename = e.FileName;
                    if (string.IsNullOrEmpty(filename) && !string.IsNullOrEmpty(e.FilePath))
                        filename = e.FilePath.Substring(e.FilePath.LastIndexOf('/') + 1);
                    tcs?.SetResult(new FileData(e.FilePath, filename, stream));
                };

                cancelledHandler = (s, e) =>
                {
                    var tcs = Interlocked.Exchange(ref _completionSource, null);
                    FilePickerActivity.FilePickCancelled -= cancelledHandler;
                    tcs?.SetResult(null);
                };

                FilePickerActivity.FilePickCancelled += cancelledHandler;
                FilePickerActivity.FilePicked += handler;
            }
            catch (Exception ex)
            {
                //Debug.Write(exAct);
            }
            return _completionSource.Task;
        }

        private int GetRequestId()
        {
            int id = _requestId;

            if (_requestId == int.MaxValue)
                _requestId = 0;
            else
                _requestId++;

            return id;
        }

        /*public async Task<bool> SaveFile(FileData fileToSave)
        {
            try
            {
                var myFile = new File(Android.OS.Environment.ExternalStorageDirectory, fileToSave.FileName);

                if (myFile.Exists())
                    return true;

                var fos = new FileOutputStream(myFile.Path);

                fos.Write(fileToSave.DataArray);
                fos.Close();

                return true;
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex.Message);
                return false;
            }
        }

        public void OpenFile(File fileToOpen)
        {
            var uri = Android.Net.Uri.FromFile(fileToOpen);
            var intent = new Intent();
            var mime = IOUtil.GetMimeType(uri.ToString());

            intent.SetAction(Intent.ActionView);
            intent.SetDataAndType(uri, mime);
            intent.SetFlags(ActivityFlags.NewTask);

            _context.StartActivity(intent);
        }

        public void OpenFile(string fileToOpen)
        {
            var myFile = new File(Android.OS.Environment.ExternalStorageState, fileToOpen);

            OpenFile(myFile);
        }

        public async void OpenFile(FileData fileToOpen)
        {
            var myFile = new File(Android.OS.Environment.ExternalStorageState, fileToOpen.FileName);

            if (!myFile.Exists())
                await SaveFile(fileToOpen);

            OpenFile(fileToOpen);
        }*/
    }
}