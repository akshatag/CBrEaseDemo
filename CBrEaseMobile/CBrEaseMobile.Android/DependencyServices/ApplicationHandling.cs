﻿using CBrEaseMobile.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(CBrEaseMobile.Droid.DependencyServices.ApplicationHandling))]
namespace CBrEaseMobile.Droid.DependencyServices
{
    public class ApplicationHandling : IApplication
    {
        public void Exit()
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
            //var activity = (Activity)Android.App.Application.Context;
            //activity.FinishAffinity();
        }
    }
}