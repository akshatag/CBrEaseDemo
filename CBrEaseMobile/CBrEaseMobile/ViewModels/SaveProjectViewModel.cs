﻿using System.Windows.Input;
using CBrEaseMobile.Services;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    class SaveProjectViewModel : ViewModelBase
    {
        public ICommand SaveFileCommand { get; set; }
        public ICommand DismissPopup { get; set; }

        private string _fileName;
        public string FileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                OnPropertyChanged("FileName");
            }
        }

        public SaveProjectViewModel()
        {
            SaveFileCommand = new Command(SaveProjectFile);
            DismissPopup = new Command(CloseThisPopup);

            FileName = CbreaseData?.ProjectInfo.BridgeId;
        }

        private void CloseThisPopup()
        {
            PopupNavigation.Instance.PopAsync();
        }

        private void SaveProjectFile()
        {
            if (!IsLicenseValid)
            {
                CloseThisPopup();
                ToastService.ShowLongToast("It seems like your license has expired. Please purchase/download 'Pro' version to avail this feature.");
                return;
            }

            if (string.IsNullOrWhiteSpace(FileName))
            {
                ToastService.ShowErrorMessage("Please give a suitable name to the file");
            }
            else
            {
                var extension = "cbz";
                FileReadWrite.WriteInCbzFile(CbreaseData, FileName + "." + extension, false);
                CloseThisPopup();
            }
        }
    }
}
