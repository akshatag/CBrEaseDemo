﻿using CBrEaseMobile.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    internal class SavedFilesViewModel : ViewModelBase
    {
        private ObservableCollection<Tuple<string, string, DateTime>> _fileList = new ObservableCollection<Tuple<string, string, DateTime>>();
        public ObservableCollection<Tuple<string, string, DateTime>> FileList
        {
            get { return _fileList; }
            set
            {
                _fileList = value;
                OnPropertyChanged("FileList");
            }
        }

        private bool _isFileCountZero;
        public bool IsFileCountZero
        {
            get { return _isFileCountZero; }
            set
            {
                _isFileCountZero = value;
                OnPropertyChanged("IsFileCountZero");
            }
        }

        internal void SetCbzFilePath(string item2)
        {
            CbzFilePath = item2;
            var fileStream = DependencyService.Get<IFileHandling>().GetFileStream(item2);

            if (fileStream != Stream.Null)
            {
                DecodeFileText(fileStream);
            }
            else
            {
                ToastService.ShowErrorMessage("Error occured while reading the file.");
            }
            PopupNavigation.Instance.PopAsync();
        }

        public SavedFilesViewModel()
        {
            var files = DependencyService.Get<IFileHandling>().GetSavedFiles();

            if(files.Count == 0)
            {
                IsFileCountZero = true;
            }

            else
            {
                IsFileCountZero = false;
                var filesOrdered = files.OrderByDescending(a => a.Item3);

                foreach (var item in filesOrdered)
                {
                    FileList.Add(item);
                }
            }            
        }
    }
}
