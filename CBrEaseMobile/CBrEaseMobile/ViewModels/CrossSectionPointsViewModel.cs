﻿using CBrEaseMobile.Models;
using CBrEaseMobile.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace CBrEaseMobile.ViewModels
{
    internal class CrossSectionPointsViewModel : ViewModelBase
    {
        static ISpeechToText speechRecognitionInstance;
        public int SelectedType = Application.Current.Properties.ContainsKey("SelectedType") ? (int)Application.Current.Properties["SelectedType"] : 1;

        private string PreviousScreen;

        #region PROPERTIES

        public string HeaderText { get; set; }
        public bool AreTabsVisible { get; set; }

        public int SelectedCrossSection { get; set; } = SelectedXSection;

        private ObservableCollection<int> _pointsList;
        public ObservableCollection<int> PointsList
        {
            get { return _pointsList; }
            set
            {
                _pointsList = value;
                OnPropertyChanged("PointsList");
            }
        }

        private int _activePoint;
        public int ActivePoint
        {
            get { return _activePoint; }
            set
            {
                _activePoint = value;
                OnPropertyChanged("ActivePoint");
            }
        }

        private int _pickerSelectedIndex;
        public int PickerSelectedIndex
        {
            get { return _pickerSelectedIndex; }
            set
            {
                _pickerSelectedIndex = value;
                OnPropertyChanged("PickerSelectedIndex");
            }
        }

        private List<int> _itemNumberList;
        public List<int> ItemNumberList
        {
            get { return _itemNumberList; }
            set
            {
                _itemNumberList = value;
                OnPropertyChanged("ItemNumberList");
            }
        }

        private int _activeItemNumber;
        public int ActiveItemNumber
        {
            get { return _activeItemNumber; }
            set
            {
                _activeItemNumber = value;
                OnPropertyChanged("ActiveItemNumber");
            }
        }

        private string _activeHorDist;
        public string ActiveHorDist
        {
            get { return _activeHorDist; }
            set
            {
                _activeHorDist = value;
                OnPropertyChanged("ActiveHorDist");
            }
        }

        private string _activeVertDist;
        public string ActiveVertDist
        {
            get { return _activeVertDist; }
            set
            {
                _activeVertDist = value;
                OnPropertyChanged("ActiveVertDist");
            }
        }

        private bool _isVerticalAdj = true;
        public bool IsVerticalAdj
        {
            get { return _isVerticalAdj; }
            set
            {
                _isVerticalAdj = value;
                OnPropertyChanged("IsVerticalAdj");
            }
        }

        private string _activeDescription;
        public string ActiveDescription
        {
            get { return _activeDescription; }
            set
            {
                _activeDescription = value;
                OnPropertyChanged("ActiveDescription");
            }
        }

        private bool _isAndroid = false;
        public bool IsAndroid
        {
            get { return _isAndroid; }
            set
            {
                _isAndroid = value;
                OnPropertyChanged("IsAndroid");
            }
        }

        private bool _isIos = false;
        public bool IsIos
        {
            get { return _isIos; }
            set
            {
                _isIos = value;
                OnPropertyChanged("IsIos");
            }
        }

        private bool _isPointOperation = false;
        public bool IsPointOperation
        {
            get { return _isPointOperation; }
            set
            {
                _isPointOperation = value;
                OnPropertyChanged("IsPointOperation");
            }
        }

        private bool _isVoiceServiceAvailable = false;
        public bool IsVoiceServiceAvailable
        {
            get { return _isVoiceServiceAvailable; }
            set
            {
                _isVoiceServiceAvailable = value;
                OnPropertyChanged("IsVoiceServiceAvailable");
            }
        }

        private bool _isPointsScreen = true;
        public bool IsPointsScreen
        {
            get => _isPointsScreen;
            set
            {
                _isPointsScreen = value;
                OnPropertyChanged("IsPointsScreen");
            }
        }

        #endregion

        #region ICOMMANDS

        public ICommand NavigateToPreviousCommand { get; set; }
        public ICommand AddPointCommand { get; set; }
        public ICommand IosVoiceCommand { get; set; }
        public ICommand OpenInstructionCommand { get; set; }
        public ICommand OpenOptionsCommand { get; set; }
        public ICommand BackToPreviousCommand { get; set; }
        public ICommand MenuTapCommand { get; set; }
        public ICommand VoiceInfoCommand { get; set; }

        #endregion

        public CrossSectionPointsViewModel()
        {
            AddPointCommand = new Command(AddActivePointsToList);
            IosVoiceCommand = new Command(VoiceRecognition);
            OpenInstructionCommand = new Command(OpenInstructionsPopup);
            BackToPreviousCommand = new Command(BackToXSection);
            MenuTapCommand = new Command(MenuTapped);
            VoiceInfoCommand = new Command(VoiceInfoTapped);

            HeaderText = "Input Points";
            AreTabsVisible = false;

            PreviousScreen = Application.Current.Properties.ContainsKey("PreviousScreen") ? Application.Current.Properties["PreviousScreen"].ToString() : "NewXSection";

            UpdateItemNumberList();
            UpdatePointsList();
            SelectLatestPointInList();

            ActiveItemNumber = 1;
            RefreshActivePoint();


            if (Device.RuntimePlatform == Device.Android)
            {
                IsAndroid = true;
                IsVoiceServiceAvailable = true;
            }
            else if (Device.RuntimePlatform == Device.iOS)
            {
                IsIos = true;
                IsVoiceServiceAvailable = true;
                try
                {
                    speechRecognitionInstance = DependencyService.Get<ISpeechToText>();
                }
                catch (Exception ex)
                {
                    IsIos = false;
                    IsVoiceServiceAvailable = false;
                }
            }
        }

        public override void Init(object args)
        {
            base.Init(args);

            if (args == null) return;

            var point = (int)args;
            ActivePoint = point;
            OnPointChanged();

        }

        private void BackToXSection()
        {
            if (PreviousScreen == "NewXSection")
            {
                NavigationService.RemoveFromNavigationStack<CrossSectionPointsViewModel>();
            }
            else
            {
                IsNewXSection = false;
                NavigationService.SetMainViewModel<CrossSectionsViewModel>();
            }
        }

        public override bool OnHardwareBackButtonPressed()
        {
            if (PreviousScreen == "NewXSection")
                return base.OnHardwareBackButtonPressed();
            IsNewXSection = false;
            NavigationService.SetMainViewModel<CrossSectionsViewModel>();
            return true;
        }

        private void UpdateItemNumberList()
        {
            ItemNumberList = new List<int>();
            CbreaseData.SubstructureInfo.Sort((s1, s2) => s1.ItemNumber.CompareTo(s2.ItemNumber));
            var i = 1;
            foreach (var item in CbreaseData.SubstructureInfo)
            {
                item.ItemNumber = i;
                ItemNumberList.Add(i++);
            }
        }

        private void UpdatePointsList()
        {
            PointsList = new ObservableCollection<int>();
            var points = CbreaseData.CrossSection.CrossSections[SelectedXSection].Points;
            foreach (var value in points)
            {
                PointsList.Add(value.Id);
            }
        }

        private void SelectLatestPointInList()
        {
            PickerSelectedIndex = PointsList.Count - 1;
        }

        private void OpenInstructionsPopup(object obj)
        {
            NavigationService.NavigateToPopup<InstructionsViewModel>();
        }

        public void RefreshActivePoint()
        {
            var points = CbreaseData.CrossSection.CrossSections[SelectedXSection].Points;
            ActivePoint = points?.Count != 0 ? points[points.Count - 1].Id + 1 : 1;
        }

        private void VoiceRecognition()
        {
            speechRecognitionInstance.Start();
            speechRecognitionInstance.textChanged += OnRecordedTextChange;
            NavigationService.NavigateToPopup<RecordingInProgressViewModel>(speechRecognitionInstance);
        }

        private void OnRecordedTextChange(object sender, EventArgsVoiceRecognition e)
        {
            if (e.IsFinal)
            {
                ShowInputVoice(e.Text);
            }
        }

        public void ShowInputVoice(string text)
        {
            var output = text?.Trim().ToLower();
            if (SelectedType == 1)
            {
                // Item Number
                var fieldValue = GetNumericFieldValue(output, "from");
                int intVal = 0;
                if (!string.IsNullOrEmpty(fieldValue) && int.TryParse(fieldValue, out intVal))
                {
                    ActiveItemNumber = intVal;
                }

                // Horizontal Distance
                fieldValue = GetNumericFieldValue(output, "horizontal");
                double dblVal = 0;
                if (!string.IsNullOrEmpty(fieldValue) && Double.TryParse(fieldValue, out dblVal))
                {
                    ActiveHorDist = dblVal.ToString();
                }

                // Vertical Distance
                fieldValue = GetNumericFieldValue(output, "vertical");
                if (!string.IsNullOrEmpty(fieldValue) && Double.TryParse(fieldValue, out dblVal))
                {
                    ActiveVertDist = dblVal.ToString();
                }

                // Add Vertical Adjustment
                bool adjVal = false;
                if (HasOffsetVoice(output, out adjVal)) IsVerticalAdj = adjVal;

                // Comments or Description
                fieldValue = GetCommentFieldValue(output, "comments");
                if (!string.IsNullOrEmpty(fieldValue))
                {
                    ActiveDescription = fieldValue;
                }
                else
                {
                    fieldValue = GetCommentFieldValue(output, "description");
                    if (!string.IsNullOrEmpty(fieldValue))
                    {
                        ActiveDescription = fieldValue;
                    }
                }
            }
        }

        private string GetNumericFieldValue(string recordedString, string field)
        {
            var strArray = recordedString.Split(' ');
            var index = strArray.IndexOf(field);
            if (index >= 0 && index < strArray.Count())
            {
                var value = strArray[index + 1];
                return VoiceService.ConvertToSingleDigit(value);
            }
            return string.Empty;
        }

        private string GetCommentFieldValue(string recordedString, string field)
        {
            var index = recordedString.IndexOf(field);
            if (index >= 0)
            {
                var value = recordedString.Substring(index + field.Length).Trim();
                return VoiceService.ConvertToSingleDigit(value);
            }
            return string.Empty;
        }

        private bool HasOffsetVoice(string recordedString, out bool value)
        {
            value = false;
            if (recordedString.IndexOf("with offset") >= 0)
            {
                value = true;
                return true;
            }
            else if (recordedString.IndexOf("no offset") >= 0)
            {
                return true;
            }
            return false;
        }

        public void AddActivePointsToList()
        {
            if (!ArePointFieldsValid())
            {
            }
            else
            {
                var calculatedValues = UpdateStationElevationCalculations();
                CbreaseData.CrossSection.CrossSections[SelectedXSection].Points.Add(calculatedValues);

                PointsList.Add(ActivePoint);

                ReorderSelectedCrossSectionPoints();

                ActivePoint = ActivePoint + 1;
                SetActiveFieldsToDefault();
                ToastService.ShowSuccessMessage("New point added successfully.");

                if (IsLicenseValid)
                    SaveChanges();
            }
        }

        private CrossSectionPoints UpdateStationElevationCalculations()
        {
            Double.TryParse(ActiveHorDist, out var horValue);
            Double.TryParse(ActiveVertDist, out var verValue);

            var originalValues = new CrossSectionPoints() { Id = ActivePoint, HorizontalDist = horValue, IsVerticalAdjEnabled = IsVerticalAdj, VerticalDist = verValue, From = ActiveItemNumber, Description = ActiveDescription };
            AssignXPointsToVariables(out CrossSectionPoints calculatedValues, originalValues, SelectedType, SelectedXSection);
            return calculatedValues;
        }

        private void SetActiveFieldsToDefault()
        {
            ActiveItemNumber = 1;
            ActiveHorDist = string.Empty;
            ActiveVertDist = string.Empty;
            IsVerticalAdj = true;
            ActiveDescription = string.Empty;
            SelectLatestPointInList();
        }

        private bool ArePointFieldsValid()
        {
            if (ActiveItemNumber == 0)
            {
                ToastService.ShowErrorMessage("Please select valid Item Number");
                return false;
            }
            return true;
        }

        private void VoiceInfoTapped()
        {
            NavigationService.NavigateToPopup<InstructionsViewModel>(Resources.Resource.SingleVoiceInstruction);
        }

        public void OnPointChanged()
        {
            IsPointOperation = true;
            var point = CbreaseData.CrossSection.CrossSections[SelectedXSection].Points.FirstOrDefault(a => a.Id == ActivePoint);
            if (point != null)
            {
                ActiveItemNumber = point.From;
                ActiveHorDist = point.HorizontalDist.ToString();
                ActiveVertDist = point.VerticalDist.ToString();
                ActiveDescription = point.Description;
                IsVerticalAdj = point.IsVerticalAdjEnabled;
            }
        }

        public void SaveThisPointToXSection()
        {
            IsPointOperation = false;

            var calculatedValues = UpdateStationElevationCalculations();
            var point = CbreaseData.CrossSection.CrossSections[SelectedXSection].Points.FirstOrDefault(a => a.Id == ActivePoint);
            if (point != null)
            {
                point.Id = calculatedValues.Id;
                point.From = calculatedValues.From;
                point.HorizontalDist = calculatedValues.HorizontalDist;
                point.VerticalDist = calculatedValues.VerticalDist;
                point.Description = calculatedValues.Description;
                point.Station = calculatedValues.Station;
                point.StationString = calculatedValues.StationString;
                point.Elevation = calculatedValues.Elevation;
                point.ElevationString = calculatedValues.ElevationString;
                point.IsVerticalAdjEnabled = calculatedValues.IsVerticalAdjEnabled;
                point.AddVerticalAdj = calculatedValues.AddVerticalAdj;
            }
            ReorderSelectedCrossSectionPoints();
            RefreshActivePoint();
            SetActiveFieldsToDefault();

            if (IsLicenseValid)
                SaveChanges();
        }

        private void SaveChanges()
        {
            if (Device.RuntimePlatform == Device.Android)
                FileReadWrite.WriteInCbzFile(CbreaseData, "", true, CbzFilePath);
            else if (Device.RuntimePlatform == Device.iOS)
            {
                var extension = "cbz";
                FileReadWrite.WriteInCbzFile(CbreaseData, CbreaseData.ProjectInfo.BridgeId + "." + extension, true);
            }
        }
    }
}
