﻿using CBrEaseMobile.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Input;
using CBrEaseMobile.Models;
using SkiaSharp;
using Xamarin.Forms;
using System.IO;
using System.Globalization;

namespace CBrEaseMobile.ViewModels
{
    internal class ViewModelBase : INotifyPropertyChanged
    {
        public static ApplicationData CbreaseData;

        public static bool IsLicenseValid { get; set; } = false;
        public static bool IsNewXSection;
        public static int SelectedXSection = 0;
        public static bool IsFirstTime = true;
        public static string CbzFilePath = string.Empty;

        public SKImageInfo DataInfo;
        public SKCanvas DataCanvas;

        public ICommand AddNewXSectionCommand { get; set; }

        public ViewModelBase()
        {
            NavigationService = new NavigationService();
            AddNewXSectionCommand = new Command(NavigateToNewXSection);
        }

        public virtual void Init(object args)
        {

        }

        public virtual bool OnHardwareBackButtonPressed()
        {
            return false;
        }

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public void OnPropertyChanged<T>(Expression<Func<T>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            if (PropertyChanged != null && memberExpression != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(memberExpression.Member.Name));
            }
        }

        #endregion

        public NavigationService NavigationService { get; set; }

        public async void ExitApplication()
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Exit", "Close Application?", "Yes", "No");
            if (!answer)
                return;
            DependencyService.Get<IApplication>().Exit();
        }

        public void MenuTapped()
        {
            NavigationService?.NavigateToPopup<OptionsViewModel>();
        }

        public void SummaryTapped()
        {
            //var selectedTab = Application.Current.Properties.ContainsKey("SelectedTab") ? Application.Current.Properties["SelectedTab"].ToString() : "1";
            //if (selectedTab == "1") return;
            Application.Current.Properties["SelectedTab"] = "1";
            NavigationService?.SetMainViewModel<ProjectInfoViewModel>();
        }

        public void ChartTapped()
        {
            //var selectedTab = Application.Current.Properties.ContainsKey("SelectedTab") ? Application.Current.Properties["SelectedTab"].ToString() : "1";
            //if (selectedTab == "2") return;
            //Application.Current.Properties["SelectedTab"] = "2";
            NavigationService?.NavigateToAsync<ChartViewModel>();
        }

        public void XSectionTapped()
        {
            //var selectedTab = Application.Current.Properties.ContainsKey("SelectedTab") ? Application.Current.Properties["SelectedTab"].ToString() : "1";
            //if (selectedTab == "3") return;
            Application.Current.Properties["SelectedTab"] = "3";
            IsNewXSection = false;
            SelectedXSection = 0;
            NavigationService?.SetMainViewModel<CrossSectionsViewModel>();
        }

        public void NavigateToNewXSection()
        {
            IsNewXSection = true;
            NavigationService?.NavigateToPopup<DateSelectorViewModel>();
            //NavigationService?.NavigateToAsync<CrossSectionsViewModel>();
        }

        #region CANVAS AND CHART DRAWING

        public void DrawBridge()
        {
            SKPaint grayPaint = new SKPaint
            {
                Style = SKPaintStyle.StrokeAndFill,
                Color = SKColors.Gray,

            };
            SKPaint darkGrayPaint = new SKPaint
            {
                Style = SKPaintStyle.StrokeAndFill,
                Color = SKColors.Gray,

            };
            SKPaint blackBorder = new SKPaint
            {
                Style = SKPaintStyle.StrokeAndFill,
                Color = SKColors.Black,

            };
            var i = 0;
            foreach (var subItem in CbreaseData.SubstructureInfo)
            {
                DrawSubstructureItem(subItem);

                foreach (var item in ColumnPoints)
                {
                    var path = new SKPath();
                    path.AddPoly(item);
                    DataCanvas.DrawPath(path, grayPaint);

                    //canvas.DrawPoints(SKPointMode.Polygon, item, blackBorder);
                }

                foreach (var item in FootingPoints)
                {
                    var path = new SKPath();
                    path.AddPoly(item);
                    DataCanvas.DrawPath(path, grayPaint);

                    //canvas.DrawPoints(SKPointMode.Polygon, item, blackBorder);
                }

                foreach (var item in TremieSealPoints)
                {
                    var path = new SKPath();
                    path.AddPoly(item);
                    DataCanvas.DrawPath(path, darkGrayPaint);

                    //canvas.DrawPoints(SKPointMode.Polygon, item, blackBorder);
                }

                foreach (var item in PilePoints)
                {
                    var path = new SKPath();
                    path.AddPoly(item);
                    DataCanvas.DrawPath(path, grayPaint);

                    //canvas.DrawPoints(SKPointMode.Polygon, item, blackBorder);
                }

                foreach (var item in SubElementRectangles)
                {
                    /*g.DrawRectangle(blackPen, item);
                    g.FillRectangle(new SolidBrush(Color.FromArgb(subTrans, col.Color)), item);*/
                }
                ColumnPoints.Clear();
                FootingPoints.Clear();
                TremieSealPoints.Clear();
                PilePoints.Clear();
                //SubElementRectangles.Clear();
                //subBrush.Dispose();

                //Create Bridge Labels as Annotations if they don't already exist

                var name = $"{subItem.Name} {++i}";


                SKPaint textPaint = new SKPaint()
                {
                    Style = SKPaintStyle.Fill,
                    Color = SKColors.Black,
                    TextSize = 25
                };
                string text = name;
                float textWidth = textPaint.MeasureText(text);
                var y = subItem.ItemBottomElevation;
                var x = subItem.EStation;
                var pt = P(x, y);
                DataCanvas.DrawText(text, pt.X - 30, pt.Y + 30, textPaint);
            }

            CreateSuperstructure();

            var superPath = new SKPath();
            superPath.AddPoly(BridgePolygon);
            DataCanvas.DrawPath(superPath, grayPaint);

        }

        public int YBuffer { get; set; }
        public int XBuffer { get; set; }
        public int[] WBuffer { get; set; }

        public double DetermineDomain()
        {
            var stations = new List<double>();
            stations.AddRange(from xItem in CbreaseData.CrossSection.CrossSections from pt in xItem.Points select Convert.ToDouble(pt.Station));
            stations.AddRange(from subItem in CbreaseData.SubstructureInfo
                              select Convert.ToDouble(UnformatStationString(subItem.StationString)));
            if (WBuffer == null)
            {
                WBuffer = new[] { 0, 0, 0, 0 };
                WBuffer[0] = -10;
                WBuffer[1] = -10;
            }
            MinS = stations.Min(i => i) + (WBuffer[0]) + XOffset;
            var maxS = stations.Max(i => i) - (WBuffer[1]) + XOffset;
            return maxS - MinS;
        }

        public double DetermineRange()
        {
            var elevations = new List<double>();
            elevations.AddRange(from xItem in CbreaseData.CrossSection.CrossSections from pt in xItem.Points select Convert.ToDouble(pt.Elevation));
            elevations.AddRange(from pt in CbreaseData.StationDepthList select pt.Elevation);
            elevations.AddRange(from item in CbreaseData.SubstructureInfo select item.ItemBottomElevation);

            MinE = elevations.Min(i => i);
            var maxE = elevations.Max(i => i);
            var range = maxE - MinE;
            YBuffer = (int)(0.15 * range);
            if (WBuffer[2] == 0 && WBuffer[3] == 0)
            {
                WBuffer[2] = -YBuffer;
                WBuffer[3] = -YBuffer;
            }
            MinE += (WBuffer[2]) * 1.2 + YOffset;
            maxE += -(WBuffer[3]) + YOffset;
            return maxE - MinE;
        }

        public string UnformatStationString(string sta)
        {
            if (sta != null)
            {
                if (!sta.Contains("+") || sta == "") return sta;
                var staString = sta.Split('+');
                var unformattedStation = (staString[0] + staString[1]);
                return unformattedStation;
            }
            return sta;

        }

        public SKPoint[] BridgePolygon { get; set; }

        //public ChartColumnList colList = new ChartColumnList();
        //public ChartFootingList ftgList = new ChartFootingList();
        //public ChartTremieSealList tremieList = new ChartTremieSealList();
        //public ChartPileList pileList = new ChartPileList();

        public List<SKPoint[]> ColumnPoints = new List<SKPoint[]>();
        public List<SKPoint[]> FootingPoints = new List<SKPoint[]>();
        public List<SKPoint[]> TremieSealPoints = new List<SKPoint[]>();
        public List<SKPoint[]> PilePoints = new List<SKPoint[]>();
        public List<Rectangle> SubElementRectangles = new List<Rectangle>();

        public void CreateSuperstructure()
        {
            var numberOfPoints = CbreaseData.StationDepthList.Count;
            BridgePolygon = new SKPoint[numberOfPoints * 2];
            var bItem = CbreaseData.SubstructureInfo[0];
            var lItem = CbreaseData.SubstructureInfo[CbreaseData.SubstructureInfo.Count - 1];
            var beginBridge = CbreaseData.StationDepthList[0].Station +
                              Math.Tan(Convert.ToDouble(bItem.BridgeSkew) * Math.PI / 180) * CbreaseData.CrossSlopeHorizDist;
            var endBridge = CbreaseData.StationDepthList[numberOfPoints - 1].Station +
                            Math.Tan(Convert.ToDouble(lItem.BridgeSkew) * Math.PI / 180) * CbreaseData.CrossSlopeHorizDist;
            var bridgeLength = endBridge - beginBridge;
            var ptSpacing = bridgeLength / numberOfPoints;

            for (var i = 0; i < numberOfPoints; i++)
            {
                var sta = CbreaseData.StationDepthList[i].Station;
                var elev = CbreaseData.StationDepthList[i].Elevation;
                BridgePolygon[i] = P(sta, elev);
            }
            for (var i = numberOfPoints; i > 0; i--)
            {
                var sta = CbreaseData.StationDepthList[i - 1].Station;
                var elev = CbreaseData.StationDepthList[i - 1].Elevation;
                var depthIndex = CbreaseData.StationDepthList.FindIndex(x => x.Station >= sta);
                if (depthIndex >= CbreaseData.StationDepthList.Count) return;
                var depth = CbreaseData.StationDepthList[depthIndex].Depth;
                BridgePolygon[numberOfPoints * 2 - i] = P(sta, elev - depth);
            }
        }

        public double Sta;
        public double MinY = double.PositiveInfinity;
        public double MaxY = double.NegativeInfinity;

        public void DrawSubstructureItem(SubstructureItem subItem)
        {
            subItem.EStation = subItem.LStation + CbreaseData.CrossSlopeHorizDist * Math.Tan(Convert.ToDouble(subItem.BridgeSkew) * Math.PI / 180);
            Sta = subItem.EStation;
            var maxWidth = FindMaxItemWidth(subItem);
            FindMaxItemHeight(subItem);

            switch (subItem.FoundationType)
            {
                case "SF":
                    CreateColumn(subItem);
                    CreateFooting(subItem);
                    break;
                case "SFS":
                    CreateColumn(subItem);
                    CreateFooting(subItem);
                    CreateTremieSeal(subItem);
                    break;
                case "PC":
                    CreateColumn(subItem);
                    CreateFooting(subItem);
                    CreatePiles(subItem);
                    break;
                case "PCS":
                    CreateColumn(subItem);
                    CreateFooting(subItem);
                    CreateTremieSeal(subItem);
                    CreatePiles(subItem);
                    break;
                case "PE":
                    CreateColumn(subItem);
                    break;
                case "Sft":
                    break;
                case "?":
                    CreateColumn(subItem);
                    break;
            }
        }

        private static double FindMaxItemWidth(SubstructureItem subItem)
        {
            //Find maximum bridge item width
            double itemWidth = 0;
            double width = 0;

            if (subItem.ColumnTopWidth != "")
            {
                width = Convert.ToDouble(subItem.ColumnTopWidth);
                itemWidth = (width > itemWidth ? width : itemWidth);
            }
            if (subItem.ColumnBottomWidth != "")
            {
                width = Convert.ToDouble(subItem.ColumnBottomWidth);
                itemWidth = (width > itemWidth ? width : itemWidth);
            }
            if (subItem.FootingWidth != "")
            {
                width = Convert.ToDouble(subItem.FootingWidth);
                itemWidth = (width > itemWidth ? width : itemWidth);
            }
            if (subItem.PileSpacing != "" && subItem.PileWidth != "" && subItem.PileNumber != "")
            {
                width = Convert.ToDouble(subItem.PileWidth) * Convert.ToDouble(subItem.PileNumber) +
                        Convert.ToDouble(subItem.PileSpacing) * (Convert.ToDouble(subItem.PileNumber) - 1);
                itemWidth = (width > itemWidth ? width : itemWidth);
            }
            return itemWidth;
        }

        private void FindMaxItemHeight(SubstructureItem subItem)
        {
            if (subItem.FootingBottomElevation != "")
            {
                var bof = Convert.ToDouble(subItem.FootingBottomElevation);
                var tof = Convert.ToDouble(subItem.FootingTopElevation);
                MinY = bof < MinY ? bof : MinY;
                MaxY = bof > MaxY ? tof : MaxY;
            }

            if (subItem.TremieSealBottomElevation != "")
            {
                var tof = Convert.ToDouble(subItem.FootingTopElevation);
                var bot = Convert.ToDouble(subItem.TremieSealBottomElevation);
                MaxY = bot > MaxY ? tof : MaxY;
            }

            if (subItem.PileTipElevation == "") return;
            var pte = Convert.ToDouble(subItem.PileTipElevation);
            MinY = pte < MinY ? pte : MinY;
            MaxY = pte > MaxY ? pte : MaxY;
        }

        public void CreateColumn(SubstructureItem subItem)
        {
            var colWid = Convert.ToDouble(subItem.ColumnBottomWidth);
            var yPxl = FindYPxlChart(subItem.ColumnTopElevation);
            var xPxl = FindXPxlChart(subItem.ColumnTopWidth, Sta, "Left");
            var topLeft = new SKPoint(xPxl, yPxl);
            xPxl = FindXPxlChart(subItem.ColumnTopWidth, Sta, "Right");
            var topRight = new SKPoint(xPxl, yPxl);
            if (subItem.FoundationType == "PE")
                subItem.ColumnBottomElevation = subItem.ItemBottomElevation = Convert.ToDouble(subItem.PileTipElevation);

            if (subItem.FoundationType == "?")
            {
                var sta = subItem.LStation + colWid / 2.0;
                double elev = 0;
                double lastSta = 0;
                double lastElev = 0;

                /*foreach (InterSection section in XSectionList)
                {
                    if (!section.Visible) continue;

                    lastSta = section.Data.Max(i => i.Station);
                    lastElev = section.Data.Find(i => i.Station >= lastSta).Elevation;

                    if (lastSta <= sta)
                    {
                        elev = lastElev;
                    }
                    else
                    {
                        elev = section.Data.Find(i => i.Station >= sta).Elevation;
                    }
                    break;
                }*/
                subItem.ColumnBottomElevation = subItem.ItemBottomElevation = elev;
            }


            yPxl = FindYPxlChart(subItem.ColumnBottomElevation);
            xPxl = FindXPxlChart(subItem.ColumnBottomWidth, Sta, "Right");
            var bottomRight = new SKPoint(xPxl, yPxl);

            if (subItem.FoundationType == "?")
            {
                var sta = subItem.LStation - colWid / 2.0;
                double elev = 0;
                double lastSta = 0;
                double lastElev = 0;

                /*foreach (InterSection section in XSectionList)
                {
                    if (!section.Visible) continue;

                    lastSta = section.Data.Max(i => i.Station);
                    lastElev = section.Data.Find(i => i.Station >= lastSta).Elevation;

                    if (lastSta <= sta)
                    {
                        elev = lastElev;
                    }
                    else
                    {
                        elev = section.Data.Find(i => i.Station >= sta).Elevation;
                    }
                    break;
                }*/
                subItem.ColumnBottomElevation = subItem.ItemBottomElevation = elev;
            }

            yPxl = FindYPxlChart(subItem.ColumnBottomElevation);
            xPxl = FindXPxlChart(subItem.ColumnBottomWidth, Sta, "Left");
            var bottomLeft = new SKPoint(xPxl, yPxl);

            var colPtList = new List<SKPoint> { topLeft, topRight, bottomRight, bottomLeft };
            var columnPoints = colPtList.ToArray();
            ColumnPoints.Add(columnPoints);
        }

        public int FindYPxlChart(double elev)
        {
            var ypxl = P(0, elev).Y;
            return (int)ypxl;
        }

        public int FindXPxlChart(string itemWidth, double sta, string side)
        {
            int xPxl = 0;
            if (itemWidth == "") return (int)P(sta, 0).X;
            var wid = Convert.ToDouble(itemWidth);
            if (side == "Left")
            {
                xPxl = (int)P(sta - wid / 2, 0).X;
            }

            else if (side == "Right")
            {
                xPxl = (int)P(sta + wid / 2, 0).X;
            }
            return xPxl;
        }

        public double MinE { get; set; }
        public double MinS { get; set; }
        public double Xscale { get; set; }
        public double Yscale { get; set; }
        public float XOffset { get; set; }
        public float YOffset { get; set; }

        public SKPoint MaxP { get; set; }
        public SKPoint MinP { get; set; }

        private SKPoint P(double x, double y)
        {
            var xf = (float)(x * Xscale);
            var yf = (float)(y * Yscale);
            var plotArea = DataInfo;
            var aPoint = new SKPoint
            {
                X = xf - (float)(MinS * Xscale),
                Y = (float)plotArea.Height + (-yf + (float)(MinE * Yscale))
            };
            return aPoint;
        }

        private void CreateFooting(SubstructureItem subItem)
        {
            if (subItem.FootingBottomElevation == "") return;

            var topF = subItem.FootingTopElevation;
            var widF = subItem.FootingWidth;
            var botF = Convert.ToDouble(subItem.FootingBottomElevation);
            var yPxl = FindYPxlChart(topF);
            var xPxl = FindXPxlChart(widF, Sta, "Left");
            var topL = new SKPoint(xPxl, yPxl);
            xPxl = FindXPxlChart(widF, Sta, "Right");
            var topR = new SKPoint(xPxl, yPxl);
            yPxl = FindYPxlChart(botF);
            xPxl = FindXPxlChart(widF, Sta, "Right");
            var botR = new SKPoint(xPxl, yPxl);
            xPxl = FindXPxlChart(widF, Sta, "Left");
            var botL = new SKPoint(xPxl, yPxl);
            var footPtList = new List<SKPoint> { topL, topR, botR, botL };
            var footPoints = footPtList.ToArray();
            FootingPoints.Add(footPoints);
        }

        private void CreateTremieSeal(SubstructureItem subItem)
        {
            if (subItem.TremieSealBottomElevation == "") return;
            var topS = Convert.ToDouble(subItem.FootingBottomElevation);
            var widF = subItem.FootingWidth;
            var botS = Convert.ToDouble(subItem.TremieSealBottomElevation);
            var yPxl = FindYPxlChart(topS);
            var xPxl = FindXPxlChart(widF, Sta, "Left");
            var topL = new SKPoint(xPxl, yPxl);
            xPxl = FindXPxlChart(widF, Sta, "Right");
            var topR = new SKPoint(xPxl, yPxl);
            yPxl = FindYPxlChart(botS);
            xPxl = FindXPxlChart(widF, Sta, "Right");
            var botR = new SKPoint(xPxl, yPxl);
            xPxl = FindXPxlChart(widF, Sta, "Left");
            var botL = new SKPoint(xPxl, yPxl);
            var sealPtList = new List<SKPoint> { topL, topR, botR, botL };
            var sealPoints = sealPtList.ToArray();
            TremieSealPoints.Add(sealPoints);
        }

        private void CreatePiles(SubstructureItem subItem)
        {
            int yMaxPxl;
            int yMinPxl;
            int xMaxPxl;
            int xMinPxl;
            subItem.UsePileSymbol = subItem.PileWidth == "" || subItem.PileSpacing == "" || subItem.PileNumber == "";

            int numPiles = 1;
            double pileSpace = 1.0f;

            double pileWidth = subItem.UsePileSymbol == true ? 1 : Convert.ToDouble(subItem.PileWidth);

            SKPoint topLeft;
            SKPoint topRight;
            SKPoint bottomLeft;
            SKPoint bottomRight;
            var sta = subItem.EStation;
            double xVal;
            switch (subItem.FoundationType)
            {
                case "PC":
                    yMaxPxl = FindYPxlChart(Convert.ToDouble(subItem.FootingBottomElevation));
                    yMinPxl = FindYPxlChart(Convert.ToDouble(subItem.PileTipElevation));
                    if (!subItem.UsePileSymbol)
                    {
                        numPiles = (int)Convert.ToDouble(subItem.PileNumber);
                        pileSpace = Convert.ToDouble(subItem.PileSpacing);
                    }

                    for (var i = 1; i <= numPiles; i++)
                    {
                        xVal = sta - ((numPiles * pileWidth) + (pileSpace * (numPiles - 1))) / 2 + (pileWidth / 2) + (i - 1) * (pileWidth + pileSpace);
                        xMinPxl = FindXPxlChart(pileWidth.ToString(), xVal, "Left");
                        xMaxPxl = FindXPxlChart(pileWidth.ToString(), xVal, "Right");
                        topLeft = new SKPoint(xMinPxl, yMaxPxl);
                        topRight = new SKPoint(xMaxPxl, yMaxPxl);
                        bottomLeft = new SKPoint(xMinPxl, yMinPxl);
                        bottomRight = new SKPoint(xMaxPxl, yMinPxl);
                        var pile = new[] { topLeft, topRight, bottomRight, bottomLeft };
                        PilePoints.Add(pile);
                    }
                    break;
                case "PCS":
                    yMaxPxl = FindYPxlChart(Convert.ToDouble(subItem.TremieSealBottomElevation));
                    yMinPxl = FindYPxlChart(Convert.ToDouble(subItem.PileTipElevation));
                    if (!subItem.UsePileSymbol)
                    {
                        numPiles = (int)Convert.ToDouble(subItem.PileNumber);
                        pileSpace = Convert.ToDouble(subItem.PileSpacing);
                    }
                    for (var i = 1; i <= numPiles; i++)
                    {
                        xVal = sta - ((numPiles * pileWidth) + (pileSpace * (numPiles - 1))) / 2 + (pileWidth / 2) + (i - 1) * (pileWidth + pileSpace);
                        xMinPxl = FindXPxlChart(subItem.PileWidth, xVal, "Left");
                        xMaxPxl = FindXPxlChart(subItem.PileWidth, xVal, "Right");
                        topLeft = new SKPoint(xMinPxl, yMaxPxl);
                        topRight = new SKPoint(xMaxPxl, yMaxPxl);
                        bottomLeft = new SKPoint(xMinPxl, yMinPxl);
                        bottomRight = new SKPoint(xMaxPxl, yMinPxl);
                        var pile = new[] { topLeft, topRight, bottomRight, bottomLeft };
                        PilePoints.Add(pile);
                    }
                    break;
                case "PE":

                    yMaxPxl = FindYPxlChart(Convert.ToDouble(subItem.ColumnBottomElevation));
                    yMinPxl = FindYPxlChart(Convert.ToDouble(subItem.PileTipElevation));
                    xVal = sta - pileWidth / 2d;
                    xMinPxl = FindXPxlChart("", xVal, null);
                    xMaxPxl = FindXPxlChart(pileWidth.ToString(), xVal + pileWidth / 2d, "Right");
                    topLeft = new SKPoint(xMinPxl, yMaxPxl);
                    topRight = new SKPoint(xMaxPxl, yMaxPxl);
                    bottomLeft = new SKPoint(xMinPxl, yMinPxl);
                    bottomRight = new SKPoint(xMaxPxl, yMinPxl);
                    var pileExt = new[] { topLeft, topRight, bottomRight, bottomLeft };
                    PilePoints.Add(pileExt);
                    break;
                case "Sft":
                    yMaxPxl = FindYPxlChart(Convert.ToDouble(subItem.ColumnTopElevation));
                    yMinPxl = FindYPxlChart(Convert.ToDouble(subItem.PileTipElevation));
                    break;
                case "?":
                    break;
            }
        }

        public void DrawChart(bool isPointsScreen = false)
        {
            var xWindow = DataInfo.Width;
            var yWindow = DataInfo.Height;
            var xS = DetermineDomain();
            var yE = DetermineRange();
            Yscale = yWindow / yE;
            Xscale = xWindow / xS;
            var maxS = MinS + xS;
            var maxE = MinE + yE;
            MinP = P(MinS, MinE);
            MaxP = P(maxS, maxE);
            if (CbreaseData.CrossSection.CrossSections.Count != XSectionList.Count)
            {
                if (!isPointsScreen) CreateInterSections();
                else CreateInterSectionsForPointsScreen();
            }
            else
            {
                UpdateInterSections();
            }
            DrawAxes(maxE, yE, xS);
        }

        public InterSectionsList XSectionList = new InterSectionsList();

        public void CreateInterSections()
        {
            XSectionList.Clear();
            var j = 0;
            foreach (var section in CbreaseData.CrossSection.CrossSections)
            {
                if (j == Colors.Count) j = 0;
                var interSection = new InterSection { Name = section.BaseDateSecondary, Color = Colors[j++] };
                foreach (var pt in section.Points)
                {
                    interSection.Data.Add(new InterSectionData { Elevation = pt.Elevation, Station = pt.Station });
                }
                XSectionList.Add(interSection);
            }
        }

        public void CreateInterSectionsForPointsScreen()
        {
            XSectionList.Clear();
            var j = 0;
            var i = 0;
            foreach (var section in CbreaseData.CrossSection.CrossSections)
            {
                if (j == Colors.Count) j = 0;
                var interSection = new InterSection { Name = section.BaseDateSecondary, Color = Colors[j++], Visible = i == SelectedXSection };
                foreach (var pt in section.Points)
                {
                    interSection.Data.Add(new InterSectionData { Elevation = pt.Elevation, Station = pt.Station });
                }
                XSectionList.Add(interSection);
                i++;
            }
        }

        private void UpdateInterSections()
        {
            var j = 0;
            foreach (var section in CbreaseData.CrossSection.CrossSections)
            {
                XSectionList[j].Data.Clear();
                foreach (var pt in section.Points)
                {
                    XSectionList[j].Data.Add(new InterSectionData { Elevation = pt.Elevation, Station = pt.Station });
                }
                j++;
            }
        }

        public ColorsList Colors = new ColorsList
        {
            SKColors.Red,
            SKColors.Orange,
            SKColors.Green,
            SKColors.Blue,
            SKColors.Purple,
            SKColors.Chocolate,
            SKColors.DarkBlue,
            SKColors.Black,
            SKColors.DarkGray
        };

        private void DrawAxes(double ygrid, double yE, double xS)
        {
            var axesPen = new SKPaint
            {
                Style = SKPaintStyle.StrokeAndFill,
                Color = SKColors.LightSteelBlue
            };

            var yMagnitude = (int)Math.Round(yE / 10 / 10.0) * 10;
            int xMagnitude;

            var j = 10;
            while (Math.Abs(yMagnitude) <= 0)
            {
                yMagnitude = (int)Math.Round(yE / j);
                j *= 10;
            }

            for (var i = 20; i >= 0; i--)
            {
                var min = MinE > 0 ? ((int)(MinE / yMagnitude) + 1) * yMagnitude : MinE - MinE % yMagnitude;
                var elev = (min + i * yMagnitude);
                var pt = P(MinS, elev);
                DataCanvas.DrawLine(MaxP.X, pt.Y, pt.X, pt.Y, axesPen);
                var elevString = Math.Round(min + i * yMagnitude).ToString();

                SKPaint textPaint = new SKPaint()
                {
                    Style = SKPaintStyle.Fill,
                    Color = SKColors.Black,
                    TextSize = 25
                };

                var elevHeight = textPaint.MeasureText(elevString);
                var point = P(MinS, elev);
                DataCanvas.DrawText(elevString, point.X, point.Y - elevHeight / 2, textPaint);
            }

            var xInterval = xS / 10;

            if (xInterval > 0 && xInterval <= 7.5)
            {
                xMagnitude = 5;
            }
            else if (xInterval > 7.5 && xInterval <= 17.5)
            {
                xMagnitude = 10;
            }
            else if (xInterval > 17.5 && xInterval <= 37.5)
            {
                xMagnitude = 25;
            }
            else if (xInterval > 37.5 && xInterval <= 75)
            {
                xMagnitude = 50;
            }
            else if (xInterval > 75 && xInterval <= 150)
            {
                xMagnitude = 100;
            }
            else
            {
                xMagnitude = 200;
            }

            for (var i = 100; i >= 0; i--)
            {
                var min = MinS > 0 ? ((int)(MinS / xMagnitude) + 1) * xMagnitude : MinS - MinS % xMagnitude;
                var station = min + i * xMagnitude;
                var pt = P(station, ygrid);
                DataCanvas.DrawLine(pt.X, DataInfo.Height, pt.X, pt.Y, axesPen);

                SKPaint textPaint = new SKPaint()
                {
                    Style = SKPaintStyle.Fill,
                    Color = SKColors.Black,
                    TextSize = 25
                };

                var stationString = FormatStationStringNoDecimal((min + i * xMagnitude).ToString());
                var stringLength = textPaint.MeasureText(stationString);
                var point = P(station, MinE);
                DataCanvas.DrawText(stationString, point.X - stringLength / 2, point.Y - 15, textPaint);
            }
        }

        public string FormatStationStringNoDecimal(string sta)
        {
            if (sta.Contains("+") || sta == "") return sta;
            var formattedStation = (Math.Truncate(Convert.ToDouble(sta) / 100)) + "+" +
                                      (Convert.ToDouble(sta) % 100).ToString("00");
            return formattedStation;
        }

        public void DrawCrossSections()
        {
            //CROSS SECTIONS
            var index = 0;

            var ptList = new List<List<SKPoint>>();

            foreach (var xItem in XSectionList)
            {
                var ind = XSectionList.IndexOf(xItem);
                ptList.Add(new List<SKPoint>());
                if (!XSectionList[ind].Visible) continue;
                foreach (var pt in xItem.Data)
                {
                    var y1 = Convert.ToDouble(pt.Elevation);
                    var x1 = Convert.ToDouble(pt.Station);
                    if ((int)x1 == 0 && (int)y1 == 0)
                        continue;
                    var pxl1 = P(x1, y1);
                    ptList[ind].Add(pxl1);
                }

                //var color = Color.Green;
                var xsPen = new SKPaint
                {
                    Style = SKPaintStyle.StrokeAndFill,
                    Color = xItem.Color,
                    StrokeWidth = 5
                };

                foreach (var pt in ptList[ind])
                {
                    var y = 2;
                    var x = 2;
                    var z = 2;
                    var ptRect = SKRect.Create((int)(pt.X - z), (int)(pt.Y - z), (int)(z * 2), (int)(2 * z));
                    DataCanvas.DrawRect(ptRect, xsPen);
                    //g.FillRectangle(new SolidBrush(color), ptRect);
                }

                //markers on the xs lines
                var y2 = 0d;
                var x2 = 0d;
                foreach (var pt in xItem.Data)
                {
                    var x1 = x2;
                    var y1 = y2;
                    y2 = pt.Elevation;
                    x2 = pt.Station;
                    if ((int)x1 == 0 && (int)y1 == 0)
                        continue;
                    var pxl1 = P(x1, y1);
                    var pxl2 = P(x2, y2);
                    DataCanvas.DrawLine(pxl1.X, pxl1.Y, pxl2.X, pxl2.Y, xsPen);
                }
            }
        }

        #endregion

        #region CROSS SECTION POINTS CALCULATION

        public void AssignXPointsToVariables(out CrossSectionPoints calculatedValues, CrossSectionPoints originalValues, int SelectedType, int SelectedIndex)
        {
            calculatedValues = new CrossSectionPoints();
            calculatedValues.Id = originalValues.Id;
            calculatedValues.From = originalValues.From;
            calculatedValues.Description = originalValues.Description;
            switch (SelectedType)
            {
                case 1:
                    calculatedValues.HorizontalDist = originalValues.HorizontalDist;
                    calculatedValues.HorizontalDist = Math.Round(calculatedValues.HorizontalDist, 2);
                    calculatedValues.VerticalDist = originalValues.VerticalDist;
                    calculatedValues.VerticalDist = Math.Round(calculatedValues.VerticalDist, 2);
                    calculatedValues.IsVerticalAdjEnabled = originalValues.IsVerticalAdjEnabled;
                    calculatedValues.AddVerticalAdj = originalValues.IsVerticalAdjEnabled ? "Yes" : "No";
                    calculatedValues.Station = CalculateEdgeStation(calculatedValues);
                    calculatedValues.StationString = FormatStationString(calculatedValues.Station.ToString());
                    calculatedValues.Elevation = CalculateXElevation(CbreaseData.CrossSection.CrossSections[SelectedIndex], calculatedValues);
                    calculatedValues.ElevationString = calculatedValues.Elevation.ToString("F2");
                    break;
                case 2:
                    calculatedValues.Station = CalculateEdgeStation(calculatedValues);
                    calculatedValues.StationString = FormatStationString(calculatedValues.Station.ToString());
                    calculatedValues.Elevation = originalValues.Elevation;
                    calculatedValues.ElevationString = calculatedValues.Elevation.ToString("F2");
                    break;
                case 3:
                    calculatedValues.HorizontalDist = originalValues.HorizontalDist;
                    calculatedValues.HorizontalDist = Math.Round(calculatedValues.HorizontalDist, 2);
                    calculatedValues.Station = CalculateEdgeStation(calculatedValues);
                    calculatedValues.StationString = FormatStationString(calculatedValues.Station.ToString());
                    calculatedValues.Elevation = originalValues.Elevation;
                    calculatedValues.ElevationString = calculatedValues.Elevation.ToString("F2");
                    break;
            }
        }

        public double CalculateXElevation(CrossSections xs, CrossSectionPoints pt)
        {
            if (xs.HasConstantElevation)
            {
                var vertAdj = pt.IsVerticalAdjEnabled ? xs.VerticalAdjustment : 0;
                pt.Elevation = xs.Elevation - vertAdj - pt.VerticalDist;
            }
            else
            {
                var elevCL = GetCenterLineElev(pt.Station);
                var crossSlope = GetCrossSlope(pt.Station);
                var elevEdge = elevCL + crossSlope * CbreaseData.CrossSlopeHorizDist;
                var vertAdj = pt.IsVerticalAdjEnabled ? xs.VerticalAdjustment : 0;
                pt.Elevation = elevEdge + xs.VerticalDistance - vertAdj - pt.VerticalDist;
            }
            return pt.Elevation;
        }

        public double GetCenterLineElev(double ptSta)
        {
            double sta;
            double elev = 0;

            switch (CbreaseData.Alignment.TypeOfAlignment)
            {
                case 1:
                    elev = CbreaseData.LGrade.Elevation;
                    break;

                case 2:
                    sta = ptSta;
                    elev = CbreaseData.CGrade.Elevation + (sta - CbreaseData.CGrade.Station) * CbreaseData.CGrade.Grade;
                    break;

                case 3:
                    sta = ptSta;
                    var lastVCIndex = CbreaseData.VertCurveList.Count - 1;
                    var lastSta = CbreaseData.VertCurveList[lastVCIndex].EvcStation;

                    for (var v = 0; v <= lastVCIndex; v++)
                    {
                        var bvcSta = CbreaseData.VertCurveList[v].BvcStation;
                        var evcSta = CbreaseData.VertCurveList[v].EvcStation;
                        if (sta <= bvcSta)
                        {
                            return CbreaseData.VertCurveList[v].BvcElevation - (bvcSta - sta) * CbreaseData.VertCurveList[v].BvcGrade / 100;
                        }
                        else if (sta >= bvcSta && sta <= evcSta)
                        {
                            var r = ((CbreaseData.VertCurveList[v].EvcGrade - CbreaseData.VertCurveList[v].BvcGrade)) / (evcSta - bvcSta);
                            return CbreaseData.VertCurveList[v].BvcElevation + ((sta - bvcSta) * (CbreaseData.VertCurveList[v].BvcGrade) / 100) +
                                   (sta - bvcSta) * (sta - bvcSta) * r / 200F;
                        }
                        else if (sta > lastSta)
                        {
                            elev = CbreaseData.VertCurveList[lastVCIndex].EvcElevation +
                                   (sta - CbreaseData.VertCurveList[lastVCIndex].EvcStation) * CbreaseData.VertCurveList[lastVCIndex].EvcGrade / 100;
                        }
                    }
                    break;
            }
            return elev;
        }

        public double GetCrossSlope(double sta)
        {
            var slope = 0d;
            if (CbreaseData.CrossSlopeType == "Constant")
            {
                return CbreaseData.ConstCrossSlope;
            }
            else if (CbreaseData.CrossSlopeType == "Varying Cross Slope")
            {
                for (int i = 0; i < CbreaseData.VCSList.Count; i++)
                {
                    var cs = CbreaseData.VCSList[i];
                    if (sta <= cs.Station && i == 0)
                    {
                        slope = cs.Slope;
                        break;
                    }
                    else if (sta >= cs.Station && i == CbreaseData.VCSList.Count - 1)
                    {
                        slope = cs.Slope;
                        break;
                    }
                    else if (sta >= cs.Station)
                    {
                        var nxtcs = CbreaseData.VCSList[i + 1];
                        slope = cs.Slope + (nxtcs.Slope - cs.Slope) / (nxtcs.Station - cs.Station) * (sta - cs.Station);
                        break;
                    }


                }
            }
            return slope / 100;


        }

        public double CalculateEdgeStation(CrossSectionPoints xD)
        {
            var bItem = xD.From;
            foreach (var item in CbreaseData.SubstructureInfo)
            {
                if (item.ItemNumber == bItem)
                    bItem = CbreaseData.SubstructureInfo.IndexOf(item);
            }
            var station = UnformatStationString(CbreaseData.SubstructureInfo[bItem].StationString);
            var sta = Convert.ToDouble(station);
            var skew = Convert.ToDouble(CbreaseData.SubstructureInfo[bItem].BridgeSkew);
            sta += Math.Tan(Math.PI / 180 * skew) * CbreaseData.CrossSlopeHorizDist + xD.HorizontalDist;
            sta = Math.Round(sta, 2);
            return sta;
        }

        public string FormatStationString(string sta)
        {
            if (sta.Contains("+") || sta == "") return sta;
            var formattedStation = (Math.Truncate(Convert.ToDouble(sta) / 100)) + "+" +
                                   (Convert.ToDouble(sta) % 100).ToString("00.0");
            return formattedStation;
        }

        public void ReorderCrossSectionPoints()
        {
            var i = 0;
            foreach (var xsection in CbreaseData.CrossSection.CrossSections)
            {
                CbreaseData.CrossSection.CrossSections[i].Points = xsection.Points?.OrderBy(a => a.Station).ToList();
                if (xsection.Points != null)
                    for (int j = 0; j < xsection.Points.Count; j++)
                    {
                        var xsectionPoints = CbreaseData.CrossSection.CrossSections[i].Points;
                        if (xsectionPoints != null)
                            xsectionPoints[j].Id = j + 1;
                    }
                i++;
            }
        }

        public void ReorderSelectedCrossSectionPoints()
        {
            var sortedList = CbreaseData.CrossSection.CrossSections[SelectedXSection].Points?.OrderBy(a => a.Station).ToList();
            CbreaseData.CrossSection.CrossSections[SelectedXSection].Points = sortedList;
            if (sortedList != null)
                for (int j = 0; j < sortedList.Count; j++)
                {
                    var xsectionPoints = CbreaseData.CrossSection.CrossSections[SelectedXSection].Points;
                    if (xsectionPoints != null)
                        xsectionPoints[j].Id = j + 1;
                }
        }

        #endregion

        #region FILE DECODE

        string _group = string.Empty;
        string[] _strVal;
        string _readLine = string.Empty;

        public void DecodeFileText(Stream data)
        {
            bool isResultValid = false;
            CbreaseData = new ApplicationData();

            CbreaseData.ProjectInfo = new ProjectInfoModel();

            CbreaseData.Alignment = new VerticalAlignment();
            CbreaseData.LGrade = new LevelGrade();
            CbreaseData.CGrade = new ConstantGrade();
            CbreaseData.VertCurveList = new VerticalCurveList();

            CbreaseData.VCSList = new VaryCrossSlopeList();

            CbreaseData.BridgeSuperstructure = new Superstructure();
            CbreaseData.VSDList = new VSDList();

            CbreaseData.SubstructureInfo = new List<SubstructureItem>();

            CbreaseData.CrossSection = new CrossSectionModel();
            CbreaseData.CrossSection.CrossSections = new List<CrossSections>();

            CbreaseData.StationDepthList = new StationDepthList();

            using (StreamReader sr = new StreamReader(data))
            {
                try
                {

                    _readLine = sr.ReadLine();
                    _strVal = _readLine.Split('=');
                    if (_strVal[0] == "Version") CbreaseData.CbzVersion = _strVal[1];
                    else CbreaseData.CbzVersion = "1.0";

                    while (sr.EndOfStream != true)
                    {
                        _readLine = sr.ReadLine();
                        if (_readLine.IndexOf("#", StringComparison.Ordinal) == 0)
                            _group = _readLine;
                        else if (_readLine.IndexOf("=", StringComparison.Ordinal) > 0)
                        {
                            _strVal = _readLine.Split('=');
                            AssignValueToVariable(sr, _group, _strVal[0], _strVal[1]);
                        }
                    }
                    isResultValid = true;
                }

                catch (Exception ex)
                {
                    _group = string.Empty;
                    _readLine = string.Empty;
                    ToastService.ShowErrorMessage(@"The file cannot be opened, or was not completely read.");
                    isResultValid = false;
                }
            }
            if (isResultValid)
            {
                NavigationService?.SetMainViewModel<ProjectInfoViewModel>();
            }
        }

        public void AssignValueToVariable(StreamReader sr, string group, string name, string val)
        {

            switch (group)
            {
                case "#PROJECT INFORMATION#":
                    ReadInProjectData(group, name, val);
                    break;
                case "#VERTICAL ALIGNMENT DATA#":
                    ReadInVerticalAlignment(sr, name, val);
                    break;
                case "#CROSS SLOPE DATA#":
                    ReadInCrossSlope(sr, name, val);
                    break;
                case "#SUPERSTRUCTURE DATA#":
                    ReadInSuperstructure(sr, name, val);
                    break;
                case "#SUBSTRUCTURE DATA#":
                    ReadInSubstructure(sr, name, val);
                    break;
                case "#CROSS SECTION DATA#":
                    ReadInCrossSection(sr, name, val);
                    break;
                case "#CHART DATA#":
                    if (name == "Number of Saved Charts")
                    {
                        CbreaseData.NumberOfSavedCharts = val;
                        UpdateCalculations();
                        _readLine = sr.ReadLine();
                        CbreaseData.SavedChartData = sr.ReadToEnd();
                    }

                    break;
            }
        }

        public void ReadInProjectData(string @group, string name, string val)
        {
            switch (name)
            {
                case "Bridge Number":
                    CbreaseData.ProjectInfo.BridgeId = val;
                    break;
                case "Bridge Name":
                    CbreaseData.ProjectInfo.BridgeName = val;
                    break;
                case "Location":
                    CbreaseData.ProjectInfo.BridgeLocation = val;
                    break;
                case "X-Section Side":
                    CbreaseData.ProjectInfo.XSectionSide = val;
                    break;
                case "Unit System":
                    CbreaseData.ProjectInfo.UnitSystem = val;
                    break;
                case "Original User":
                    CbreaseData.ProjectInfo.OriginalUser = val;
                    break;
                case "Current User":
                    CbreaseData.ProjectInfo.CurrentUser = val;
                    break;
            }
        }

        public void ReadInVerticalAlignment(StreamReader sr, string name, string val)
        {
            CbreaseData.Alignment.TypeOfAlignment = Convert.ToInt32(val);
            switch (CbreaseData.Alignment.TypeOfAlignment)
            {
                case 1:
                    _readLine = sr.ReadLine();
                    var levelVals = _readLine.Split('=');
                    CbreaseData.LGrade.Elevation = Convert.ToDouble(levelVals[1]);
                    break;

                case 2:
                    _readLine = sr.ReadLine();
                    var cgVals = _readLine.Split('=');
                    CbreaseData.CGrade.Station = Convert.ToDouble(UnformatStationString(cgVals[1]));
                    _readLine = sr.ReadLine();
                    cgVals = _readLine.Split('=');
                    CbreaseData.CGrade.Elevation = Convert.ToDouble(cgVals[1]);
                    _readLine = sr.ReadLine();
                    cgVals = _readLine.Split('=');
                    CbreaseData.CGrade.Grade = Convert.ToDouble(cgVals[1]);
                    break;

                case 3:
                    _readLine = sr.ReadLine();
                    var vcLine = _readLine.Split('=');
                    var totalVCs = Convert.ToInt32(vcLine[1]);
                    var _header = sr.ReadLine();//ignore
                    CbreaseData.VertCurveList.Clear();
                    for (var vcNum = 1; vcNum <= totalVCs; vcNum++)
                    {
                        _readLine = sr.ReadLine();
                        var vc = _readLine.Split(',');

                        CbreaseData.VertCurveList.Add(new VerticalCurve(Convert.ToInt32(vc[0]), Convert.ToDouble(UnformatStationString(vc[1])),
                            Math.Round(Convert.ToDouble(vc[2]), 2), Convert.ToDouble(vc[3]) * 100, Convert.ToDouble(UnformatStationString(vc[4])),
                            Math.Round(Convert.ToDouble(vc[5]), 2), Convert.ToDouble(vc[6]) * 100));
                    }
                    break;
            }
        }

        public void ReadInCrossSlope(StreamReader sr, string name, string val)
        {
            CbreaseData.CrossSlopeHorizDist = Convert.ToDouble(val);
            _readLine = sr.ReadLine();
            string[] csLine = _readLine.Split('=');
            CbreaseData.CrossSlopeType = csLine[1];
            switch (CbreaseData.CrossSlopeType)
            {
                case "Constant":
                    _readLine = sr.ReadLine();
                    csLine = _readLine.Split('=');
                    CbreaseData.ConstCrossSlope = Convert.ToDouble(csLine[1]);
                    break;
                case "Varying Cross Slope":
                    _readLine = sr.ReadLine();

                    string[] vsLine = _readLine.Split('=');
                    int numVsPts = Convert.ToInt32(vsLine[1]);
                    _readLine = sr.ReadLine();
                    for (int i = 0; i < numVsPts; i++)
                    {
                        _readLine = sr.ReadLine();

                        vsLine = _readLine.Split(',');
                        var sta = UnformatStationString(vsLine[0]);

                        var slope = Convert.ToDouble(vsLine[1]);

                        CbreaseData.VCSList.Add(new VaryingCrossSlope(Convert.ToDouble(sta),
                            Convert.ToDouble(slope)));
                    }
                    break;
            }
        }

        public void ReadInSuperstructure(StreamReader sr, string name, string val)
        {
            CbreaseData.BridgeSuperstructure.Thickness = Convert.ToDouble(val);
            _readLine = sr.ReadLine();
            var superLine = _readLine.Split('=');
            var vsdNum = Convert.ToInt32(superLine[1]);
            if (vsdNum <= 0)
            {
                CbreaseData.BridgeSuperstructure.StructuralDepthType = "Constant Structural Depth";
                return;
            }
            CbreaseData.BridgeSuperstructure.StructuralDepthType = "Variable Structural Depth";
            CbreaseData.VSDList = new VSDList();
            for (var i = 0; i < vsdNum; i++)
            {
                var VKeyPtsList = new VariableTemplate();
                _readLine = sr.ReadLine();
                superLine = _readLine.Split('=');
                VKeyPtsList.NameOfVsd = superLine[1];
                _readLine = sr.ReadLine();
                superLine = _readLine.Split('=');
                var keyPtNum = Convert.ToInt32(superLine[1]);
                _readLine = sr.ReadLine();//header
                for (var j = 0; j < keyPtNum; j++)
                {
                    _readLine = sr.ReadLine();
                    var keyPtLine = _readLine.Split(',');
                    var vKeyPts = new VariablePts(Convert.ToDouble(keyPtLine[0]), Convert.ToDouble(keyPtLine[1]));
                    VKeyPtsList.Add(vKeyPts);
                }
                CbreaseData.VSDList.Add(VKeyPtsList);
            }
        }

        public void ReadInSubstructure(StreamReader sr, string name, string val)
        {
            var subLine = _readLine.Split('=');
            var numSubItems = Convert.ToInt32(subLine[1]);
            _readLine = sr.ReadLine();
            if (numSubItems <= 0) return;
            for (var i = 0; i < numSubItems; i++)
            {
                _readLine = sr.ReadLine();
                subLine = _readLine.Split('=');
                var subItem = new SubstructureItem(Convert.ToInt32(subLine[1]));

                _readLine = sr.ReadLine();
                subLine = _readLine.Split('=');
                subItem.Name = subLine[1];

                _readLine = sr.ReadLine();
                subLine = _readLine.Split('=');
                subItem.StationString = subLine[1];
                subItem.LStation = Convert.ToDouble(UnformatStationString(subItem.StationString));

                _readLine = sr.ReadLine();
                subLine = _readLine.Split('=');
                subItem.BridgeSkew = subLine[1];

                subItem.EStation = subItem.LStation + CbreaseData.CrossSlopeHorizDist * Math.Tan(Convert.ToDouble(subItem.BridgeSkew) * Math.PI / 180);

                _readLine = sr.ReadLine();
                subLine = _readLine.Split('=');
                subItem.VsdLeft = subLine[1];

                _readLine = sr.ReadLine();
                subLine = _readLine.Split('=');
                subItem.VsdRight = subLine[1];

                _readLine = sr.ReadLine();
                subLine = _readLine.Split('=');
                subItem.FoundationType = subLine[1];

                _readLine = sr.ReadLine();//column header
                _readLine = sr.ReadLine();//width header
                _readLine = sr.ReadLine();
                subLine = _readLine.Split(',');
                subItem.ColumnTopWidth = subLine[0].Replace("\t", "");
                subItem.ColumnBottomWidth = subLine[1];
                subItem.ColumnTopElevation = Convert.ToDouble(subLine[2]);

                var type = subItem.FoundationType;
                _readLine = sr.ReadLine();//footing header
                _readLine = sr.ReadLine();//footing dimensions header
                _readLine = sr.ReadLine();
                subLine = _readLine.Split(',');
                if (type != "PE" && type != "?")
                {
                    subItem.FootingWidth = subLine[0].Replace("\t", "");
                    subItem.FootingThickness = subLine[1];
                    subItem.FootingTopElevation = Convert.ToDouble(subLine[2]);
                    subItem.ColumnBottomElevation = subItem.FootingTopElevation;
                    subItem.FootingBottomElevation = subLine[3];
                    subItem.TremieSealBottomElevation = subLine[4];
                }
                _readLine = sr.ReadLine();//pile header
                _readLine = sr.ReadLine();//pile dimensions header
                _readLine = sr.ReadLine();
                subLine = _readLine.Split(',');
                subItem.PileWidth = subLine[0].Replace("\t", "");
                subItem.PileTipElevation = subLine[1];
                subItem.PileNumber = subLine[2];
                subItem.PileSpacing = subLine[3];

                if (type.Contains("P"))
                    subItem.ItemBottomElevation = Convert.ToDouble(subItem.PileTipElevation);
                else if (type.Contains("FS"))
                    subItem.ItemBottomElevation = Convert.ToDouble(subItem.TremieSealBottomElevation);
                else if (type == "SF")
                    subItem.ItemBottomElevation = Convert.ToDouble(subItem.FootingBottomElevation);
                else if (type.Contains("?"))
                    subItem.ItemBottomElevation = GetCenterLineElev(subItem.EStation);

                if (type == "PE" && subItem.PileWidth == "") subItem.PileWidth = subItem.ColumnBottomWidth;

                CbreaseData.SubstructureInfo.Add(subItem);
            }
        }

        private void ReadInCrossSection(StreamReader sr, string name, string val)
        {
            CbreaseData.CrossSection.CrossSections = new List<CrossSections>();
            var line = _readLine.Split('=');
            var numXSections = Convert.ToInt32(line[1]);
            _readLine = sr.ReadLine();
            if (numXSections <= 0) return;

            for (var i = 0; i < numXSections; i++)
            {
                var xItem = new CrossSections();
                do
                {
                    _readLine = sr.ReadLine();
                } while (_readLine == "");

                line = _readLine?.Split('=');
                var tempDateTime = Convert.ToDateTime(line?[1], CultureInfo.InvariantCulture);
                xItem.BaseDate = tempDateTime.ToString("MM/dd/yyyy");
                xItem.BaseDateSecondary = tempDateTime.ToString("MM/dd/yy");
                _readLine = sr.ReadLine();
                line = _readLine?.Split('=');
                xItem.TypeString = line?[1];

                switch (xItem.TypeString)
                {
                    case "Channel X-Section":
                        xItem.Type = 0;
                        break;
                    case "Engineering Plans":
                        xItem.Type = 1;
                        break;
                    case "Engineering Survey":
                        xItem.Type = 2; break;
                }

                _readLine = sr.ReadLine();
                line = _readLine?.Split('=');
                xItem.Comments = line?[1];

                _readLine = sr.ReadLine();
                line = _readLine?.Split('=');
                xItem.CollectedBy = line?[1];

                _readLine = sr.ReadLine();
                line = _readLine?.Split('=');
                xItem.VerticalDistance = Convert.ToDouble(line?[1]);

                _readLine = sr.ReadLine();
                line = _readLine?.Split('=');
                xItem.VerticalAdjustment = Convert.ToDouble(line?[1]);

                _readLine = sr.ReadLine();
                line = _readLine?.Split('=');
                //xItem.HasReferenceFace = Convert.ToBoolean(line?[1]);
                xItem.HasReferenceFace = new[] { "yes", "true" }.Any(a => line[1].ToLower().Contains(a));

                _readLine = sr.ReadLine();
                line = _readLine?.Split('=');
                //xItem.HasConstantElevation = Convert.ToBoolean(line?[1]);
                xItem.HasConstantElevation = new[] { "yes", "true" }.Any(a => line[1].ToLower().Contains(a));

                if (xItem.HasConstantElevation)
                {
                    _readLine = sr.ReadLine();
                    line = _readLine?.Split('=');
                    xItem.Elevation = Convert.ToDouble(line?[1]);
                }
                _readLine = sr.ReadLine();

                _readLine = sr.ReadLine();
                line = _readLine?.Split('=');
                var numXPoints = Convert.ToInt32(line?[1]);

                _readLine = sr.ReadLine();
                var j = 0;

                while (j < numXPoints)
                {
                    var xD = new CrossSectionPoints();
                    _readLine = sr.ReadLine();
                    var lineDesc = _readLine.Split('"');
                    line = lineDesc[0].Split(',');
                    //xD.Id = Convert.ToInt32(line?[0].Replace("\t", ""));
                    xD.From = Convert.ToInt32(line?[1]);
                    xD.HorizontalDist = Convert.ToDouble(line?[2]);
                    xD.VerticalDist = Convert.ToDouble(line?[3]);
                    //xD.IsVerticalAdjEnabled = line?[4].ToLower() == "true";
                    xD.IsVerticalAdjEnabled = new[] { "yes", "true" }.Any(a => line[4].ToLower().Contains(a));
                    xD.AddVerticalAdj = xD.IsVerticalAdjEnabled ? "Yes" : "No";
                    xD.Description = lineDesc[1];
                    line = lineDesc[2].Split(',');
                    xD.StationString = line?[1];
                    xD.ElevationString = line?[2];
                    xD.Station = Convert.ToDouble(xD.StationString);
                    xD.Elevation = Convert.ToDouble(xD.ElevationString);

                    xItem.Points.Add(xD);
                    j++;

                }
                CbreaseData.CrossSection.CrossSections.Add(xItem);
            }
            //Reorder_XList(AppData.CrossSection.CrossSections);
        }

        //private void Reorder_XList(List<CrossSections> XList)
        //{
        //    var xCnt = XList.Count;
        //    bool swapFound = false;

        //    do
        //        for (int x = 0; x < xCnt - 1; x++)
        //        {
        //            swapFound = false;
        //            if (Convert.ToDateTime(XList[x + 1].BaseDate) < Convert.ToDateTime(XList[x].BaseDate))
        //            {
        //                //XList.Swap(XList, x, x + 1);
        //                swapFound = true;
        //                break;
        //            }
        //        }
        //    while (swapFound == true);
        //    //comboXSDate.Items.Clear();
        //    foreach (var xs in XList)
        //    {
        //        if (xs.BaseDate != null)
        //        {
        //            //comboXSDate.Items.Add(xs.Date);
        //        }
        //    }
        //}

        public void UpdateCalculations()
        {
            CalculateAllXSections();
            MakeStationDepthList();
            Update_All_Column_Elevations();

            ReorderCrossSectionPoints();
        }

        public void CalculateAllXSections()
        {
            foreach (var section in CbreaseData.CrossSection.CrossSections)
            {
                foreach (var pt in section.Points)
                {
                    //if (section.TypeString == "Channel X-Section")
                    //{
                    pt.Station = CalculateEdgeStation(pt);
                    pt.StationString = FormatStationString(pt.Station.ToString());
                    pt.Elevation = CalculateXElevation(section, pt);
                    pt.ElevationString = pt.Elevation.ToString("0.0");
                    //}
                    //else if (section.TypeString == "Engineering Survey")
                    //{
                    //}
                    //else if (section.TypeString == "Engineering Plans")
                    //{
                    //    pt.Station = CalculateEdgeStation(pt);
                    //    pt.StationString = FormatStationString(pt.Station.ToString());
                    //}
                }
            }
        }

        public void MakeStationDepthList()
        {
            CbreaseData.StationDepthList.Clear();
            foreach (var subItem in CbreaseData.SubstructureInfo)
            {
                var index = CbreaseData.SubstructureInfo.IndexOf(subItem);
                var nextItem = index == CbreaseData.SubstructureInfo.Count - 1 ? CbreaseData.SubstructureInfo[index] : CbreaseData.SubstructureInfo[index + 1];
                var buffer = Convert.ToDouble(CbreaseData.SubstructureInfo[index].ColumnTopWidth) / 2;
                var subS = subItem.EStation;
                var sta1 = subS - (index == 0 ? buffer : 0);
                var nextS = nextItem.EStation;
                var sta2 = nextItem == subItem ? nextS + buffer : nextS;
                if (sta2 == sta1 && sta1 == 0) return;
                switch (CbreaseData.BridgeSuperstructure.StructuralDepthType?.Contains("Constant"))
                {
                    //Constant structural depth case
                    case true:
                        for (var i = sta1; i <= sta2; i++)
                        {
                            var point = new StationDepthPoint
                            {
                                Depth = CbreaseData.BridgeSuperstructure.Thickness,
                                Station = i
                            };
                            var elev = GetEdgeElevation(i);
                            point.Elevation = elev;
                            CbreaseData.StationDepthList.Add(point);
                        }
                        break;

                    //Variable Structural Depth Case
                    case false:
                        var vsdRight1 = subItem.VsdRight == "None" ? null : CbreaseData.VSDList[CbreaseData.VSDList.FindIndex(x => x.NameOfVsd == subItem.VsdRight)];
                        var vsdLeft2 = nextItem.VsdLeft == "None" ? null : CbreaseData.VSDList[CbreaseData.VSDList.FindIndex(x => x.NameOfVsd == nextItem.VsdLeft)];

                        double minDepth = -1;
                        double maxDepth = -1;
                        foreach (var template in CbreaseData.VSDList)
                        {
                            var newMin = template.Min(i => i.Depth);
                            if (Math.Abs(minDepth - -1) <= 0) minDepth = newMin;
                            minDepth = minDepth < newMin ? minDepth : newMin;

                            var newMax = template.Max(i => i.Depth);
                            if (Math.Abs(maxDepth - -1) <= 0) maxDepth = newMax;
                            maxDepth = maxDepth < newMax ? maxDepth : newMax;
                        }

                        var rightBool = false;
                        var leftBool = false;

                        for (var i = sta1; i <= sta2; i += (sta2 - sta1) / 20)
                        {
                            var point = new StationDepthPoint { Station = i };
                            var elev = GetEdgeElevation(i);
                            point.Elevation = elev;
                            if (InVsdRightRange(subS, point.Station, vsdRight1) && rightBool) continue;
                            if (InVsdLeftRange(nextS, point.Station, vsdLeft2) && leftBool) continue;
                            if (InVsdRightRange(subS, point.Station, vsdRight1) && !rightBool)
                            {
                                foreach (var pt in vsdRight1)
                                {
                                    point = new StationDepthPoint();
                                    point.Station = subS + pt.Distance;
                                    point.Elevation = GetEdgeElevation(point.Station);
                                    point.Depth = pt.Depth;
                                    CbreaseData.StationDepthList.Add(point);
                                }
                                subItem.ColumnTopElevation = elev - point.Depth;
                                rightBool = true;
                                continue;
                            }
                            else if (InVsdLeftRange(nextS, point.Station, vsdLeft2) && !leftBool)
                            {
                                for (var j = vsdLeft2.Count - 1; j >= 0; j--)
                                {
                                    point = new StationDepthPoint();
                                    var pt = vsdLeft2[j];
                                    point.Station = nextS - pt.Distance;
                                    point.Elevation = GetEdgeElevation(point.Station);
                                    point.Depth = pt.Depth;
                                    CbreaseData.StationDepthList.Add(point);
                                }
                                subItem.ColumnTopElevation = elev - point.Depth;
                                leftBool = true;
                                continue;
                            }
                            else if (!InVsdLeftRange(nextS, point.Station, vsdLeft2) && !InVsdRightRange(subS, point.Station, vsdRight1))
                            {
                                if (InColumnRange(subS, point.Station, Convert.ToDouble(subItem.ColumnTopWidth)))
                                {
                                    point.Depth = vsdRight1 == null ? minDepth : maxDepth;
                                    // subItem.ColumnTopElevation = elev - point.Depth;
                                }
                                else if (InColumnRange(nextS, point.Station, Convert.ToDouble(subItem.ColumnTopWidth)))
                                    point.Depth = vsdLeft2 == null ? minDepth : maxDepth;
                                else
                                    point.Depth = minDepth;
                            }
                            CbreaseData.StationDepthList.Add(point);
                        }
                        break;
                }
            }
            //DisplayDataGridItems();
        }

        public double GetEdgeElevation(double edgeSta)
        {
            var centerElevation = GetCenterLineElev(edgeSta);
            var crossSlope = GetCrossSlope(edgeSta);
            var edgeElevation = centerElevation + CbreaseData.CrossSlopeHorizDist * crossSlope;
            return edgeElevation;
        }

        public bool InVsdRightRange(double subStation, double pointStation, VariableTemplate template)
        {
            return pointStation < subStation + template?[template.Count - 1].Distance &&
                   pointStation > subStation;
        }

        public bool InVsdLeftRange(double subStation, double pointStation, VariableTemplate template)
        {
            return pointStation > subStation - template?[template.Count - 1].Distance &&
                   pointStation < subStation;
        }

        public bool InColumnRange(double subStation, double pointStation, double colWid)
        {
            return pointStation >= subStation - colWid / 2 && pointStation <= subStation + colWid / 2;
        }

        public void Update_All_Column_Elevations()
        {
            foreach (var item in CbreaseData.SubstructureInfo)
            {
                Update_Column_Elevation(item);
            }
        }

        public void Update_Column_Elevation(SubstructureItem item)
        {
            var crossSlope = GetCrossSlope(item.EStation);
            var depth = 0.0;
            if (CbreaseData.BridgeSuperstructure.StructuralDepthType == "Constant Structural Depth")
            {
                depth = CbreaseData.BridgeSuperstructure.Thickness;
            }
            else
            {
                var depthIndex = CbreaseData.StationDepthList.FindIndex(x => x.Station >= item.EStation);
                depth = CbreaseData.StationDepthList[depthIndex].Depth;
            }
            item.ColumnTopElevation = GetEdgeElevation(item.EStation) - depth;
        }

        #endregion
    }
}
