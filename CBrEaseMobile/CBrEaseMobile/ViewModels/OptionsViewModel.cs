﻿using CBrEaseMobile.Services;
using Rg.Plugins.Popup.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    internal class OptionsViewModel : ViewModelBase
    {
        public ICommand WriteToFileCommand { get; set; }
        public ICommand ExitAppCommand { get; set; }

        public OptionsViewModel()
        {
            WriteToFileCommand = new Command(SaveFile);
            ExitAppCommand = new Command(ClosePopupAndExit);
        }

        private void ClosePopupAndExit()
        {
            PopupNavigation.Instance.PopAsync();
            ExitApplication();
        }

        private void SaveFile()
        {
            PopupNavigation.Instance.PopAsync();
            if (IsLicenseValid)
                NavigationService?.NavigateToPopup<SaveProjectViewModel>();
            else
                ToastService.ShowLongToast("It seems like your license has expired. Please purchase/download 'Pro' version to avail this feature.");
        }
    }
}
