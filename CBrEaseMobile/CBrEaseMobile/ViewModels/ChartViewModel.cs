﻿using System.Windows.Input;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    internal class ChartViewModel : ViewModelBase
    {

        public string HeaderText { get; set; }
        public bool AreTabsVisible { get; set; }

        public ICommand BackToPreviousCommand { get; set; }
        public ICommand OpenOptionsCommand { get; set; }
        public ICommand MenuTapCommand { get; set; }

        private bool _isPointsScreen = false;
        public bool IsPointsScreen
        {
            get => _isPointsScreen;
            set
            {
                _isPointsScreen = value;
                OnPropertyChanged("IsPointsScreen");
            }
        }

        public ChartViewModel()
        {
            BackToPreviousCommand = new Command(BackToPreviousScreen);
            OpenOptionsCommand = new Command(OpenOptionsPopup);
            MenuTapCommand = new Command(MenuTapped);

            HeaderText = CbreaseData.ProjectInfo.BridgeId;
            AreTabsVisible = false;
        }

        private void OpenOptionsPopup()
        {
            NavigationService?.NavigateToPopup<OptionsViewModel>();
        }

        private void BackToPreviousScreen()
        {
            NavigationService.RemoveFromNavigationStack<ChartViewModel>();
        }
    }
}
