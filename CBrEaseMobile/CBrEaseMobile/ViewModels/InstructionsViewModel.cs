﻿using Rg.Plugins.Popup.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    internal class InstructionsViewModel : ViewModelBase
    {
        private string _voiceCommandText;
        public string InstructionText
        {
            get { return _voiceCommandText; }
            set
            {
                _voiceCommandText = value;
                OnPropertyChanged("VoiceCommandText");
            }
        }

        public ICommand DismissPopup { get; set; }

        public InstructionsViewModel()
        {
            DismissPopup = new Command(CloseThisPopup);
        }

        private void CloseThisPopup(object obj)
        {
            PopupNavigation.Instance.PopAsync();
        }

        public override void Init(object args)
        {
            InstructionText = args.ToString();
            base.Init(args);
        }
    }
}
