﻿using CBrEaseMobile.FilePicker;
using CBrEaseMobile.Services;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Portable.Licensing;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    internal class SelectFileViewModel : ViewModelBase
    {
        private string _appLicenseStatus;
        public string AppLicenseStatus
        {
            get { return _appLicenseStatus; }
            set
            {
                _appLicenseStatus = value;
                OnPropertyChanged("AppLicenseStatus");
            }
        }

        public ICommand BrowseFileCommand { get; set; }
        public ICommand ShowRecentFiles { get; set; }

        private FileData file;

        public SelectFileViewModel()
        {
            BrowseFileCommand = new Command(BrowseFile);
            ShowRecentFiles = new Command(ShowAllFiles);
        }

        private bool IsLicenseLoaded { get; set; } = false;

        public void InvokeLicense()
        {
            Task.Run(async () =>
            {
                var result = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
                var status = result.ContainsKey(Permission.Storage) ? result[Permission.Storage] : PermissionStatus.Unknown;

                if (status == PermissionStatus.Granted)
                {
                    var data = LoadLicense();
                    if (string.IsNullOrEmpty(data))
                    {
                        SaveLicense();
                    }
                    else
                    {
                        ExtractLicenseData(data);
                    }

                    IsLicenseLoaded = true;
                }
                else
                {
                    ShowPermissionPopup();
                }
            });
        }

        private void SaveLicense()
        {
            var LicenseStr = string.Empty;
            try
            {
                var passPhrase = string.Empty;
                var keyGenerator = Portable.Licensing.Security.Cryptography.KeyGenerator.Create();
                var keyPair = keyGenerator.GenerateKeyPair();
                var privateKey = keyPair.ToEncryptedPrivateKeyString(passPhrase);
                //var publicKey = keyPair.ToPublicKeyString();

                var license = License.New()
        .WithUniqueIdentifier(Guid.NewGuid())
        .As(LicenseType.Trial)
        .ExpiresAt(DateTime.Now.AddDays(30))
        .CreateAndSignWithPrivateKey(privateKey, passPhrase);

                LicenseStr = license.ToString();
            }
            catch (Exception ex)
            {
                ToastService.ShowErrorMessage("An error occured while generating the license.");
            }
            var result = DependencyService.Get<IFileHandling>().SaveLicense(LicenseStr);
            if (!result)
            {
                ToastService.ShowErrorMessage("An error occured while storing the license.");
            }
            else
            {
                ExtractLicenseData(LoadLicense());
            }
        }

        private string LoadLicense()
        {
            return DependencyService.Get<IFileHandling>().GetLicense();
        }

        private void ExtractLicenseData(string licenseData)
        {
            try
            {
                var license = License.Load(licenseData);
                var expirationDate = license.Expiration;
                var timeLeft = expirationDate - DateTime.Now;
                if (timeLeft > new TimeSpan())
                {
                    AppLicenseStatus = timeLeft.Days == 0 ? "Just Today" : string.Format(Resources.Resource.TrialTimeText, timeLeft.Days);
                    IsLicenseValid = true;
                }
                else
                {
                    IsLicenseValid = false;
                    AppLicenseStatus = Resources.Resource.TrialEndText;
                }
            }
            catch (Exception ex)
            {
                ToastService.ShowErrorMessage("An error occured while loading the license.");
                AppLicenseStatus = "Unable to generate license.";
                IsLicenseValid = false;
            }
        }

        private async void BrowseFile(object obj)
        {
            var result = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
            var status = result.ContainsKey(Permission.Storage) ? result[Permission.Storage] : PermissionStatus.Unknown;

            if (status == PermissionStatus.Granted)
            {
                if (!IsLicenseLoaded)
                {
                    InvokeLicense();
                }

                var fileExt = "cbz";
                file = await DependencyService.Get<IFilePicker>().PickFile();
                if (file == null)
                {
                    ToastService.ShowErrorMessage("No file selected");
                }
                else if (file.FileName.Substring(file.FileName.LastIndexOf('.') + 1)?.Trim()?.ToLower() != fileExt)
                {
                    ToastService.ShowErrorMessage("Unsupported file format");
                }
                else
                {
                    CbzFilePath = file.FilePath;
                    DecodeFileText(file.DataStream);
                }
            }
            else
            {
                ShowPermissionPopup();
            }
        }

        private void ShowPermissionPopup()
        {
            NavigationService?.NavigateToPopup<InstructionsViewModel>(Resources.Resource.PermissionRequired);
        }

        private void ShowAllFiles()
        {
            Task.Run(async () =>
            {
                var result = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
                var status = result.ContainsKey(Permission.Storage) ? result[Permission.Storage] : PermissionStatus.Unknown;

                if (status == PermissionStatus.Granted)
                {
                    if (!IsLicenseLoaded)
                    {
                        InvokeLicense();
                    }
                    NavigationService?.NavigateToPopup<SavedFilesViewModel>();
                }
                else
                {
                    ShowPermissionPopup();
                }
            });
        }
    }
}
