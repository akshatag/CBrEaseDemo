﻿using CBrEaseMobile.Models;
using CBrEaseMobile.Services;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    internal class DateSelectorViewModel : ViewModelBase
    {

        private DateTime _pickerDate = DateTime.Now;
        public DateTime PickerDate
        {
            get { return _pickerDate; }
            set
            {
                _pickerDate = value;
                OnPropertyChanged("PickerDate");
            }
        }

        public ICommand SelectDateCommand { get; set; }
        public ICommand DismissModalCommand { get; set; }

        public DateSelectorViewModel()
        {
            SelectDateCommand = new Command(SelectPickerDate);
            DismissModalCommand = new Command(DismissThisModal);
        }

        private void DismissThisModal()
        {
            PopupNavigation.Instance.PopAsync();
        }

        private void SelectPickerDate()
        {
            var selectedDate = PickerDate.ToString("MM/dd/yyyy");
            var dateSecondary = PickerDate.ToString("MM/dd/yy");
            CbreaseData.CrossSection.CrossSections.Add(new CrossSections { BaseDate = selectedDate, BaseDateSecondary = dateSecondary, Comments = string.Empty, CollectedBy = string.Empty, Type = 0, VerticalDistance = 0, VerticalAdjustment = 0, HasReferenceFace = false, HasConstantElevation = false, Elevation = 0, Points = new List<CrossSectionPoints>() });
            SelectedXSection = CbreaseData.CrossSection.CrossSections.Count - 1;
            ToastService.ShowSuccessMessage("New cross-section added successfully.");
            PopupNavigation.Instance.PopAsync();
            NavigationService?.NavigateToAsync<CrossSectionsViewModel>();
        }
    }
}
