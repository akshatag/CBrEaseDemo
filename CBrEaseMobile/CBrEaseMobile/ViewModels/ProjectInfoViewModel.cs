﻿using System.Windows.Input;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    internal class ProjectInfoViewModel : ViewModelBase
    {
        #region Properties
        public string BridgeId { get; set; }
        public string BridgeSide { get; set; }
        public string BridgeUnit { get; set; }
        public string BridgeName { get; set; }
        public string BridgeLocation { get; set; }
        public string OriginalUser { get; set; }
        public string CurrentUser { get; set; }
        public string HeaderText { get; set; }

        public bool AreTabsVisible { get; set; }

        private bool _isPointsScreen = false;
        public bool IsPointsScreen
        {
            get => _isPointsScreen;
            set
            {
                _isPointsScreen = value;
                OnPropertyChanged("IsPointsScreen");
            }
        }
        #endregion

        #region ICommands
        public ICommand MenuTapCommand { get; set; }
        public ICommand ChartTapCommand { get; set; }
        public ICommand XSectionTapCommand { get; set; }

        #endregion

        public ProjectInfoViewModel()
        {
            MenuTapCommand = new Command(MenuTapped);
            ChartTapCommand = new Command(ChartTapped);
            XSectionTapCommand = new Command(XSectionTapped);

            BridgeId = CbreaseData.ProjectInfo.BridgeId;
            BridgeName = CbreaseData.ProjectInfo.BridgeName;
            BridgeLocation = CbreaseData.ProjectInfo.BridgeLocation;

            BridgeSide = CbreaseData.ProjectInfo.XSectionSide;
            BridgeUnit = CbreaseData.ProjectInfo.UnitSystem;

            OriginalUser = CbreaseData.ProjectInfo.OriginalUser;
            CurrentUser = CbreaseData.ProjectInfo.CurrentUser;

            HeaderText = BridgeId;
            AreTabsVisible = true;
        }
    }
}
