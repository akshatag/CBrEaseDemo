﻿using CBrEaseMobile.Services;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;
using CBrEaseMobile.Models;
using System.Collections.ObjectModel;
using System;
using System.Linq;

namespace CBrEaseMobile.ViewModels
{
    internal class CrossSectionsViewModel : ViewModelBase
    {
        static ISpeechToText speechRecognitionInstance;
        public int ActiveVoiceField;
        public List<CrossSections> CrossSectionsAll;

        #region PROPERTIES
        public string HeaderText { get; set; }
        public bool AreTabsVisible { get; set; }

        private ObservableCollection<string> _crossSectionDates;
        public ObservableCollection<string> CrossSectionDates
        {
            get { return _crossSectionDates; }
            set
            {
                _crossSectionDates = value;
            }
        }

        private int _selectedDateIndex;
        public int SelectedDateIndex
        {
            get { return _selectedDateIndex; }
            set
            {
                _selectedDateIndex = value;
                OnPropertyChanged("SelectedDateIndex");
                if (SelectedDateIndex >= 0 && CrossSectionsAll.Count != 0)
                {
                    SelectedXSection = SelectedDateIndex;
                    SelectedDate = CrossSectionsAll[SelectedDateIndex].BaseDate;
                    SelectedType = CrossSectionsAll[SelectedDateIndex].Type;
                    SelectedComment = CrossSectionsAll[SelectedDateIndex].Comments;
                    SelectedCollectedBy = CrossSectionsAll[SelectedDateIndex].CollectedBy;
                    SelectedVerticalDistance = CrossSectionsAll[SelectedDateIndex].VerticalDistance;
                    SelectedVerticalAdjustment = CrossSectionsAll[SelectedDateIndex].VerticalAdjustment;
                    SelectedHasConstantElevation = CrossSectionsAll[SelectedDateIndex].HasConstantElevation;
                    SelectedHasReferenceFace = CrossSectionsAll[SelectedDateIndex].HasReferenceFace;
                    SelectedElevation = CrossSectionsAll[SelectedDateIndex].Elevation;
                    var points = CrossSectionsAll[SelectedDateIndex].Points.Count;
                    if (points != 0)
                    {
                        CurrentPage = 1;
                        TotalPages = (int)Math.Ceiling(points / 5.0);
                        LoadPointsList();
                    }
                    else
                    {
                        CurrentPage = 0;
                        TotalPages = 0;
                        SelectedPoints.Clear();
                    }
                    IsPreviousPageAvailable = false;
                    IsNextPageAvailable = TotalPages > CurrentPage;
                    if (IsPastXSections)
                    {
                        IsXSectionEditable = false;
                    }
                }

            }
        }

        private string _selectedDate;
        public string SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;
                OnPropertyChanged("SelectedDate");
            }
        }

        public ObservableCollection<string> XSectionTypes { get; set; }

        private int _selectedType;
        public int SelectedType
        {
            get { return _selectedType; }
            set
            {
                _selectedType = value;
                OnPropertyChanged("SelectedType");
                if (SelectedType >= 0)
                {
                    switch (SelectedType)
                    {
                        case 0:
                            SelectedTypeText = "X-Sections";
                            break;
                        case 1:
                            SelectedTypeText = "Survey";
                            break;
                        case 2:
                            SelectedTypeText = "Plans";
                            break;
                        default: break;
                    }
                }
            }
        }

        private double _listViewHeight;
        public double ListViewHeight
        {
            get { return _listViewHeight; }
            set
            {
                _listViewHeight = value;
                OnPropertyChanged("ListViewHeight");
            }
        }

        private string _selectedTypeText;
        public string SelectedTypeText
        {
            get { return _selectedTypeText; }
            set
            {
                _selectedTypeText = value;
                OnPropertyChanged("SelectedTypeText");
            }
        }

        private string _selectedComment;
        public string SelectedComment
        {
            get { return _selectedComment; }
            set
            {
                _selectedComment = value;
                OnPropertyChanged("SelectedComment");
            }
        }

        private string _selectedCollectedBy;
        public string SelectedCollectedBy
        {
            get { return _selectedCollectedBy; }
            set
            {
                _selectedCollectedBy = value;
                OnPropertyChanged("SelectedCollectedBy");
            }
        }

        private double _selectedVerticalDistance = 0;
        public double SelectedVerticalDistance
        {
            get { return _selectedVerticalDistance; }
            set
            {
                _selectedVerticalDistance = value;
                OnPropertyChanged("SelectedVerticalDistance");
            }
        }

        private double _selectedVerticalAdjustment = 0;
        public double SelectedVerticalAdjustment
        {
            get { return _selectedVerticalAdjustment; }
            set
            {
                _selectedVerticalAdjustment = value;
                OnPropertyChanged("SelectedVerticalAdjustment");
            }
        }

        private bool _selectedHasReferenceFace = false;
        public bool SelectedHasReferenceFace
        {
            get { return _selectedHasReferenceFace; }
            set
            {
                _selectedHasReferenceFace = value;
                OnPropertyChanged("SelectedHasReferenceFace");
                SelectedHasRefText = SelectedHasReferenceFace ? "yes" : "no";
            }
        }

        private string _selectedHasRefText;
        public string SelectedHasRefText
        {
            get { return _selectedHasRefText; }
            set
            {
                _selectedHasRefText = value;
                OnPropertyChanged("SelectedHasRefText");
            }
        }

        private bool _selectedHasConstantElevation = false;
        public bool SelectedHasConstantElevation
        {
            get { return _selectedHasConstantElevation; }
            set
            {
                _selectedHasConstantElevation = value;
                OnPropertyChanged("SelectedHasConstantElevation");
                SelectedConstText = SelectedHasConstantElevation ? "yes" : "no";
            }
        }

        private string _selectedConstText;
        public string SelectedConstText
        {
            get { return _selectedConstText; }
            set
            {
                _selectedConstText = value;
                OnPropertyChanged("SelectedConstText");
            }
        }

        private double _selectedElevation;
        public double SelectedElevation
        {
            get { return _selectedElevation; }
            set
            {
                _selectedElevation = value;
                OnPropertyChanged("SelectedElevation");
            }
        }

        private bool _isCrossSectionListNotEmpty = false;
        public bool IsCrossSectionListNotEmpty
        {
            get { return _isCrossSectionListNotEmpty; }
            set
            {
                _isCrossSectionListNotEmpty = value;
                OnPropertyChanged("IsCrossSectionListNotEmpty");
            }
        }

        private bool _isAndroid;
        public bool IsAndroid
        {
            get { return _isAndroid; }
            set
            {
                _isAndroid = value;
                OnPropertyChanged("IsAndroid");
            }
        }

        private bool _isIos;
        public bool IsIos
        {
            get { return _isIos; }
            set
            {
                _isIos = value;
                OnPropertyChanged("IsIos");
            }
        }

        private bool _isXSectionEditable;
        public bool IsXSectionEditable
        {
            get { return _isXSectionEditable; }
            set
            {
                _isXSectionEditable = value;
                OnPropertyChanged("IsXSectionEditable");
            }
        }

        private ObservableCollection<CrossSectionPoints> _selectedPoints = new ObservableCollection<CrossSectionPoints>();
        public ObservableCollection<CrossSectionPoints> SelectedPoints
        {
            get { return _selectedPoints; }
            set
            {
                _selectedPoints = value;
                OnPropertyChanged("SelectedPoints");
            }
        }

        private bool _isPastXSections;
        public bool IsPastXSections
        {
            get { return _isPastXSections; }
            set
            {
                _isPastXSections = value;
                OnPropertyChanged("IsPastXSections");
            }
        }

        private bool _isPreviousPageAvailable;
        public bool IsPreviousPageAvailable
        {
            get { return _isPreviousPageAvailable; }
            set
            {
                _isPreviousPageAvailable = value;
                OnPropertyChanged("IsPreviousPageAvailable");
            }
        }

        private bool _isNextPageAvailable;
        public bool IsNextPageAvailable
        {
            get { return _isNextPageAvailable; }
            set
            {
                _isNextPageAvailable = value;
                OnPropertyChanged("IsNextPageAvailable");
            }
        }

        private bool _isPointsScreen = false;
        public bool IsPointsScreen
        {
            get => _isPointsScreen;
            set
            {
                _isPointsScreen = value;
                OnPropertyChanged("IsPointsScreen");
            }
        }

        private int CurrentPage;
        private int TotalPages;
        #endregion

        #region ICommands
        public ICommand DeleteSelectedDateCommand { get; set; }
        public ICommand IosVoiceCommand { get; set; }

        public ICommand MenuTapCommand { get; set; }
        public ICommand SummaryTapCommand { get; set; }
        public ICommand ChartTapCommand { get; set; }

        public ICommand EditXSectionCommand { get; set; }
        public ICommand SaveXSectionCommand { get; set; }

        public ICommand SaveAndContCommand { get; set; }

        public ICommand BackToPreviousCommand { get; set; }

        public ICommand PreviousPageCommand { get; set; }
        public ICommand NextPageCommand { get; set; }

        public ICommand AddMorePointsCommand { get; set; }

        #endregion

        public CrossSectionsViewModel()
        {
            DeleteSelectedDateCommand = new Command(DeleteSelectedDateAsync);
            IosVoiceCommand = new Command(VoiceRecognition);
            MenuTapCommand = new Command(MenuTapped);
            SummaryTapCommand = new Command(SummaryTapped);
            ChartTapCommand = new Command(ChartTapped);
            EditXSectionCommand = new Command(EditCrossSection);
            SaveXSectionCommand = new Command(SaveCrossSection);
            
            SaveAndContCommand = new Command(SaveXSectionAndNavigateToPoints);
            BackToPreviousCommand = new Command(BackToPreviousScreen);

            PreviousPageCommand = new Command(ShowPreviousPage);
            NextPageCommand = new Command(ShowNextPage);

            AddMorePointsCommand = new Command(AddPointsToExisting);

            CrossSectionsAll = new List<CrossSections>();
            CrossSectionDates = new ObservableCollection<string>();
            XSectionTypes =
                new ObservableCollection<string> { "X-Section (Hor+Ver)", "Survey (Sta+El)", "Plans (Hor+El)" };

            CrossSectionsAll = CbreaseData?.CrossSection.CrossSections;

            TriggeredOnCrossSectionsChanged();

            if (IsNewXSection)
            {
                HeaderText = "Add New Cross-Section";
                AreTabsVisible = false;
                IsPastXSections = false;
                IsXSectionEditable = true;
            }
            else
            {
                HeaderText = CbreaseData.ProjectInfo.BridgeId;
                AreTabsVisible = true;
                IsPastXSections = true;
                IsXSectionEditable = false;
            }

            if (Device.RuntimePlatform == Device.Android)
            {
                IsAndroid = true;
            }
            else if (Device.RuntimePlatform == Device.iOS)
            {
                IsIos = true;
                try
                {
                    speechRecognitionInstance = DependencyService.Get<ISpeechToText>();
                }
                catch (Exception ex)
                {
                    IsIos = false;
                }
            }

            SelectedDateIndex = SelectedXSection;

        }

        private void AddPointsToExisting()
        {
            Application.Current.Properties["PreviousScreen"] = "PastXSection";
            NavigationService?.NavigateToAsync<CrossSectionPointsViewModel>();
        }

        private void ShowPreviousPage()
        {
            --CurrentPage;
            LoadPointsList();
            IsPreviousPageAvailable = CurrentPage != 1;
            IsNextPageAvailable = TotalPages > CurrentPage;
        }

        private void ShowNextPage()
        {
            ++CurrentPage;
            LoadPointsList();
            IsNextPageAvailable = TotalPages > CurrentPage;
            IsPreviousPageAvailable = true;
        }

        private void BackToPreviousScreen()
        {
            var selectedTab = Application.Current.Properties.ContainsKey("SelectedTab") ? Application.Current.Properties["SelectedTab"].ToString() : "1";
            if (selectedTab == "3")
            {
                IsNewXSection = false;
                NavigationService.SetMainViewModel<CrossSectionsViewModel>();
            }
            else
            {
                NavigationService.RemoveFromNavigationStack<CrossSectionsViewModel>();
            }
        }

        public override bool OnHardwareBackButtonPressed()
        {
            var selectedTab = Application.Current.Properties.ContainsKey("SelectedTab") ? Application.Current.Properties["SelectedTab"].ToString() : "1";
            if (selectedTab == "3")
            {
                IsNewXSection = false;
                NavigationService.SetMainViewModel<CrossSectionsViewModel>();
                return false;
            }
            else
            {
                return base.OnHardwareBackButtonPressed();
            }
        }

        private void LoadPointsList()
        {
            var listValues = CrossSectionsAll[SelectedDateIndex].Points.Skip(5 * (CurrentPage - 1)).Take(5).ToList();
            ListViewHeight = listValues.Count * 44;
            SelectedPoints.Clear();
            foreach (var item in listValues)
            {
                SelectedPoints.Add(item);
            }
        }

        private void EditCrossSection()
        {
            IsXSectionEditable = true;
        }

        private void SaveCrossSection()
        {
            SaveSelectedValues();
            IsXSectionEditable = false;
        }

        private void SaveSelectedValues()
        {
            CrossSectionsAll[SelectedDateIndex].CollectedBy = SelectedCollectedBy;
            CrossSectionsAll[SelectedDateIndex].Comments = SelectedComment;
            CrossSectionsAll[SelectedDateIndex].Type = SelectedType;
            CrossSectionsAll[SelectedDateIndex].VerticalDistance = SelectedVerticalDistance;
            CrossSectionsAll[SelectedDateIndex].VerticalAdjustment = SelectedVerticalAdjustment;
            CrossSectionsAll[SelectedDateIndex].HasConstantElevation = SelectedHasConstantElevation;
            CrossSectionsAll[SelectedDateIndex].HasReferenceFace = SelectedHasReferenceFace;
            CrossSectionsAll[SelectedDateIndex].Elevation = SelectedElevation;
        }

        private void SaveXSectionAndNavigateToPoints()
        {
            SaveSelectedValues();
            Application.Current.Properties["SelectedIndex"] = SelectedDateIndex;
            Application.Current.Properties["PreviousScreen"] = "NewXSection";
            NavigationService?.NavigateToAsync<CrossSectionPointsViewModel>();
        }

        private void VoiceRecognition(object obj)
        {
            var field = Convert.ToInt32(obj);
            ActiveVoiceField = field;
            speechRecognitionInstance.Start();
            speechRecognitionInstance.textChanged += OnRecordedTextChange;
            NavigationService.NavigateToPopup<RecordingInProgressViewModel>(speechRecognitionInstance);
        }

        private void OnRecordedTextChange(object sender, EventArgsVoiceRecognition e)
        {
            if (e.IsFinal)
            {
                ShowInputVoice(VoiceService.ConvertToSingleDigit(e.Text));
            }
        }

        public void ShowInputVoice(string text)
        {
            bool isDouble = double.TryParse(text, out var dblValue);
            if (ActiveVoiceField == 1) SelectedComment = text;
            else if (ActiveVoiceField == 2) SelectedCollectedBy = text;
            else if (ActiveVoiceField == 3) { if (isDouble) SelectedVerticalDistance = dblValue; else ToastService.ShowErrorMessage("Only numeric value allowed for this field."); }
            else if (ActiveVoiceField == 4) { if (isDouble) SelectedVerticalAdjustment = dblValue; else ToastService.ShowErrorMessage("Only numeric value allowed for this field."); }
            else if (ActiveVoiceField == 5) { if (isDouble) SelectedElevation = dblValue; else ToastService.ShowErrorMessage("Only numeric value allowed for this field."); }
        }

        private async void DeleteSelectedDateAsync(object obj)
        {
            var answer = await Application.Current.MainPage.DisplayAlert("Delete", "Are you sure to delete selected cross-section?", "Yes", "No");
            if (!answer)
                return;

            var item = CrossSectionsAll[SelectedDateIndex];
            CrossSectionsAll.Remove(item);
            if (IsNewXSection)
            {
                SelectedXSection = 0;
                BackToPreviousScreen();
            }
            else
            {
                TriggeredOnCrossSectionsChanged();
                SelectedDateIndex = 0;
            }
            ToastService.ShowSuccessMessage("Cross-section deleted successfully.");
        }

        public void TriggeredOnCrossSectionsChanged()
        {
            if (CrossSectionsAll.Count == 0)
            {
                IsCrossSectionListNotEmpty = false;
                return;
            }
            CrossSectionDates.Clear();
            foreach (var item in CrossSectionsAll)
            {
                CrossSectionDates.Add(item.BaseDate);
            }
            IsCrossSectionListNotEmpty = true;
        }

        public void EditThisPoint(int point)
        {
            Application.Current.Properties["PreviousScreen"] = "PastXSection";
            NavigationService?.NavigateToAsync<CrossSectionPointsViewModel>(point);
        }

        public void DeleteThisPoint(int id)
        {
            var item = CbreaseData.CrossSection.CrossSections[SelectedDateIndex].Points.FirstOrDefault(a => a.Id == id);
            CbreaseData.CrossSection.CrossSections[SelectedDateIndex].Points.Remove(item);
            ReorderSelectedCrossSectionPoints();
            LoadPointsList();

            var point = CrossSectionsAll[SelectedDateIndex].Points.Count;

            TotalPages = (int)Math.Ceiling(point / 5.0);

            ToastService.ShowSuccessMessage("Point has been deleted successfully.");

            if (IsLicenseValid)
                SaveChanges();
        }

        private void SaveChanges()
        {
            if (Device.RuntimePlatform == Device.Android)
                FileReadWrite.WriteInCbzFile(CbreaseData, "", true, CbzFilePath);
            else if (Device.RuntimePlatform == Device.iOS)
            {
                var extension = "cbz";
                FileReadWrite.WriteInCbzFile(CbreaseData, CbreaseData.ProjectInfo.BridgeId + "." + extension, true);
            }
        }
    }
}
