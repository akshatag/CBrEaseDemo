﻿using CBrEaseMobile.Services;
using Rg.Plugins.Popup.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace CBrEaseMobile.ViewModels
{
    internal class RecordingInProgressViewModel : ViewModelBase
    {
        private ISpeechToText ModalInstance;
        public ICommand StopVoiceInputCommand { get; set; }
        public RecordingInProgressViewModel()
        {
            StopVoiceInputCommand = new Command(StopSpeechRecognitionInstance);
        }

        private void StopSpeechRecognitionInstance(object obj)
        {
            ModalInstance.Stop();
            PopupNavigation.Instance.PopAsync();
        }

        public override void Init(object args)
        {
            var instance = args as ISpeechToText;
            ModalInstance = instance;
        }
    }
}
