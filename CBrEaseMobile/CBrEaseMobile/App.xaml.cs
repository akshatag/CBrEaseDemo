﻿using CBrEaseMobile.Services;
using CBrEaseMobile.ViewModels;
using Xamarin.Forms;

namespace CBrEaseMobile
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            Current.Properties.Clear();
            new NavigationService().SetMainViewModel<SelectFileViewModel>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
