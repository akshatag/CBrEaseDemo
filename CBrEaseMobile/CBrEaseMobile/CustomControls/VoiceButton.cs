﻿using System;
using Xamarin.Forms;

namespace CBrEaseMobile.CustomControls
{
    public class VoiceButton : Button
    {
        public Action<string> OnTextChanged { get; set; }
    }
}
