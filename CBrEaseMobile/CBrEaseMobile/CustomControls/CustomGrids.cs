﻿using Xamarin.Forms;

namespace CBrEaseMobile.CustomControls
{
    internal class ListViewGrid : Grid
    {
        public ListViewGrid()
        {
            ColumnSpacing = 3;
            RowSpacing = 0;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            VerticalOptions = LayoutOptions.CenterAndExpand;
            var row = new RowDefinition() { Height = new GridLength(1, GridUnitType.Auto) };
            RowDefinitions.Add(row);
            for (int column = 0; column < 7; column++)
            {
                var colDef = new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) };
                ColumnDefinitions.Add(colDef);
            }
        }
    }
}
