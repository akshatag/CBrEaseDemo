﻿using System;
using System.IO;

namespace CBrEaseMobile.FilePicker
{
    public class FileData
    {
        private string _fileName;
        private string _filePath;
        private readonly Stream _stream;

        public FileData()
        { }

        public FileData(string filePath, string fileName, Stream stream, Action<bool> dispose = null)
        {
            _filePath = filePath;
            _fileName = fileName;
            _stream = stream;
        }

        public Stream DataStream => _stream;

        /// <summary>
        /// Filename of the picked file
        /// </summary>
        public string FileName
        {
            get => _fileName;
            set => _fileName = value;
        }

        /// <summary>
        /// Full filepath of the picked file
        /// </summary>
        public string FilePath
        {
            get => _filePath;
            set => _filePath = value;
        }
    }
}
