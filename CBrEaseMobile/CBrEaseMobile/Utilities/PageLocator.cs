﻿using CBrEaseMobile.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Diagnostics;
using System.Reflection;
using Xamarin.Forms;

namespace CBrEaseMobile.Utilities
{
    internal class PageLocator
    {
        protected virtual ContentPage CreatePage(Type pageType)
        {
            return Activator.CreateInstance(pageType) as ContentPage;
        }

        protected virtual MasterDetailPage CreateMasterPage(Type pageType)
        {
            return Activator.CreateInstance(pageType) as MasterDetailPage;
        }

        protected virtual CarouselPage CreateCarouselPage(Type pageType)
        {
            return Activator.CreateInstance(pageType) as CarouselPage;
        }

        protected virtual PopupPage CreatePopup(Type pageType)
        {
            return Activator.CreateInstance(pageType) as PopupPage;
        }

        protected virtual ViewModelBase CreateViewModel(Type viewModelType)
        {
            return Activator.CreateInstance(viewModelType) as ViewModelBase;
        }

        protected virtual Type FindPageTypeForViewModel(Type viewModelType)
        {
            var pageTypeName = viewModelType
                .AssemblyQualifiedName
                .Replace("ViewModel", "View");

            var pageType = Type.GetType(pageTypeName);
            if (pageType == null)
                throw new ArgumentException("Can't find a page of type '" + pageTypeName + "' for ViewModel '" +
                                            viewModelType.Name +
                                            "'");

            return pageType;
        }

        public Type ResolvePageType(Type viewModelType)
        {
            return FindPageTypeForViewModel(viewModelType);
        }

        public Page ResolvePageAndViewModel(Type viewModelType, object args)
        {
            Page page = null;

            var viewModel = CreateViewModel(viewModelType);
            if (args != null)
                viewModel?.Init(args);
            try
            {
                page = ResolvePage(viewModel);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return page;
        }

        public Page ResolvePage(ViewModelBase viewModel)
        {
            var pageType = ResolvePageType(viewModel.GetType());
            var baseType = pageType.GetTypeInfo().BaseType;

            if (baseType.FullName.Equals("Xamarin.Forms.MasterDetailPage"))
            {
                var masterPage = CreateMasterPage(pageType);
                masterPage.BindingContext = viewModel;
                return masterPage as MasterDetailPage;
            }
            else if (baseType.FullName.Equals("Xamarin.Forms.CarouselPage"))
            {
                var carousel = CreateCarouselPage(pageType);
                carousel.BindingContext = viewModel;
                return carousel;
            }
            else if (baseType.FullName.Equals("Rg.Plugins.Popup.Pages.PopupPage"))
            {
                var popup = CreatePopup(pageType);
                popup.BindingContext = viewModel;
                return popup;
            }
            else
            {
                var page = CreatePage(pageType);
                page.BindingContext = viewModel;
                return page as ContentPage;
            }
        }
    }
}
