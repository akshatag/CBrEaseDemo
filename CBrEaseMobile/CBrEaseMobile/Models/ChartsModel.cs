﻿using SkiaSharp;
using System.Collections.Generic;
using Xamarin.Forms;

namespace CBrEaseMobile.Models
{
    public class Annotation
    {
        public string Type { get; set; }
        public double BegSta { get; set; }
        public double BegElev { get; set; }
        public double EndSta { get; set; }
        public double EndElev { get; set; }
        public double Size { get; set; }
        public string Attribute { get; set; }
        public bool Visible { get; set; } = true;
        public int ActiveNode { get; set; }
        public Rectangle NodeRectangle { get; set; }
    }

    public class InterSectionsList : List<InterSection> { }

    public class InterSection
    {
        public SKColor Color;
        public bool Visible = true;
        public int Thickness = 2;
        public string Name;
        public int Opacity = 255;
        public List<InterSectionData> Data = new List<InterSectionData>();
        public HydraulicList HList = new HydraulicList();
        public Hydraulics Hydraulic = new Hydraulics();
    }

    public class InterSectionData
    {
        public double Station { get; set; }
        public double Elevation { get; set; }
    }

    public class StreamStats : List<StreamLine>
    {
        public double Q { get; set; }
        public double Wsel { get; set; }
        public double Velocity { get; set; }
        public double Wet { get; set; }
        public double Area { get; set; }
        public double TopWid { get; set; }
        public double HydRad { get; set; }
        public double AveDepth { get; set; }
        public double Froude { get; set; }
    }

    public class StreamLine
    {
        public double Q { get; set; }
        public double Wsel { get; set; }
        public double Velocity { get; set; }
        public double Wet { get; set; }
        public double Area { get; set; }
        public double TopWid { get; set; }
        public double HydRad { get; set; }
        public double AveDepth { get; set; }
        public double Froude { get; set; }
        public double EndSta { get; set; }
        public double EndEl { get; set; }
        public bool Visible { get; set; } = true;
        public List<StreamLinePoints> HData { get; set; } = new List<StreamLinePoints>();

    }

    public class StreamLinePoints
    {
        public double Station { get; set; }
        public double Elevation { get; set; }
    }

    public class HydraulicList : List<Hydraulics> { }

    public class Hydraulics
    {
        public StreamStats Stats { get; set; }
        public List<StreamLine> Streams { get; set; }
        public List<Manning> NList = new List<Manning>();
        public List<Overbank> OList = new List<Overbank>();
    }

    public class Manning
    {
        public double BegSta;
        public double EndSta;
        public double Value;
    }

    public class Overbank
    {
        public double BegSta;
        public double EndSta;
        public double EndElev;
        public string Location;
    }

    public class ColorsList : List<SKColor> { }

}
