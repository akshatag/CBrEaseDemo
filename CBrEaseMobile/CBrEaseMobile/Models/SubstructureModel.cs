﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace CBrEaseMobile.Models
{
    public class SubstructureItem
    {
        public string Name { get; set; }
        public string StationString { get; set; }
        public double LStation { get; set; }
        public double EStation { get; set; }
        public string FoundationType { get; set; }
        public int ItemNumber { get; set; }
        public string BridgeSkew { get; set; }
        public string VsdLeft { get; set; }
        public string VsdRight { get; set; }
        public double ItemBottomElevation { get; set; }

        public string ColumnTopWidth { get; set; }
        public string ColumnBottomWidth { get; set; }
        public double ColumnTopElevation { get; set; }
        public double ColumnBottomElevation { get; set; }


        public string FootingWidth { get; set; }
        public string FootingThickness { get; set; }
        public double FootingTopElevation { get; set; }
        public string FootingBottomElevation { get; set; }


        public double TremieSealThickness { get; set; }
        public double TremieSealTopElevation { get; set; }
        public string TremieSealBottomElevation { get; set; }


        public string PileWidth { get; set; }
        public string PileTipElevation { get; set; }
        public string PileSpacing { get; set; }
        public string PileNumber { get; set; }
        public double PileTopElevation { get; set; }
        public bool MissingPileData { get; set; }
        public bool UsePileSymbol = false;



        public SubstructureItem(int number)
        {
            ItemNumber = number;
        }
    }

    public class ChartColumn : ChartSubItems { };
    public class ChartFooting : ChartSubItems { };
    public class ChartTremieSeal : ChartSubItems { };
    public class ChartPile : ChartSubItems { };

    public class ChartColumnList : List<ChartColumn> { }
    public class ChartFootingList : List<ChartFooting> { }
    public class ChartTremieSealList : List<ChartTremieSeal> { }
    public class ChartPileList : List<ChartPile> { }

    public class ChartSubItems
    {
        public bool Visible { get; set; } = true;
        public Color Color { get; set; } = Color.Gray;
        public bool UseMaterial { get; set; } = false;
        public string MatType { get; set; } = "None";
    }

    public class StationDepthList : List<StationDepthPoint> { }
    public class StationDepthPoint
    {
        public double Station { get; set; }
        public double Elevation { get; set; }
        public double Depth { get; set; }
    }
}
