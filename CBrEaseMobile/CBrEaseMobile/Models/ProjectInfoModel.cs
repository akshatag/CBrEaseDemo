﻿namespace CBrEaseMobile.Models
{
    public class ProjectInfoModel
    {
        public string BridgeId { get; set; }
        public string BridgeName { get; set; }
        public string BridgeLocation { get; set; }
        public string XSectionSide { get; set; }
        public string UnitSystem { get; set; }
        public string OriginalUser { get; set; }
        public string CurrentUser { get; set; }
    }
}
