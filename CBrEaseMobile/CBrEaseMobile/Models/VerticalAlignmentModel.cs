﻿using System.Collections.Generic;

namespace CBrEaseMobile.Models
{
    public class VerticalAlignment
    {
        public int TypeOfAlignment { get; set; }
    }
    public class LevelGrade : VerticalAlignment
    {
        public double Elevation { get; set; }
    }
    public class ConstantGrade : VerticalAlignment
    {
        public double Station { get; set; }
        public double Elevation { get; set; }
        public double Grade { get; set; }
    }
    public class VerticalCurveList : List<VerticalCurve>
    {
        public int Number { get; set; }
    }
    public class VerticalCurve : VerticalAlignment
    {
        public int Number { get; set; }
        public double BvcStation { get; set; }
        public double BvcElevation { get; set; }
        public double BvcGrade { get; set; }
        public double EvcStation { get; set; }
        public double EvcElevation { get; set; }
        public double EvcGrade { get; set; }

        public VerticalCurve(int number, double bVcStation, double bVcElevation, double bVcGrade,
            double eVcStation, double eVcElevation, double eVcGrade)
        {
            Number = number;
            BvcStation = bVcStation;
            BvcElevation = bVcElevation;
            BvcGrade = bVcGrade;
            EvcStation = eVcStation;
            EvcElevation = eVcElevation;
            EvcGrade = eVcGrade;
        }
    }
}
