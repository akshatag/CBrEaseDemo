﻿using SkiaSharp;
using System.Collections.Generic;

namespace CBrEaseMobile.Models
{
    public class Superstructure
    {
        public string StructuralDepthType { get; set; }
        public double BeginBridge { get; set; }
        public double EndBridge { get; set; }
        public List<SKPoint> DeckPoints { get; set; }
        public List<SKPoint> SoffitPoints { get; set; }
        public double Thickness { get; set; }
    }
    public class VSDList : List<VariableTemplate> { }
    public class VariableTemplate : List<VariablePts>
    {
        public string NameOfVsd { get; set; }
    }
    public class VariablePts
    {
        public double Distance { get; set; }
        public double Depth { get; set; }
        public VariablePts(double distance, double depth)
        {
            Distance = distance;
            Depth = depth;
        }
    }
}
