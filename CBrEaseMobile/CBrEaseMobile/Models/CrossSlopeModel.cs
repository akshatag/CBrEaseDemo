﻿using System.Collections.Generic;

namespace CBrEaseMobile.Models
{
    public class CrossSlope
    {
        public double Slope { get; set; }
    }
    public class ConstantCrossSlope : CrossSlope { }
    public class VaryingCrossSlope : CrossSlope
    {
        public double Station { get; set; }

        public VaryingCrossSlope(double station, double slope)
        {
            Station = station;
            Slope = slope;
        }
    }

    public class VaryCrossSlopeList : List<VaryingCrossSlope>
    {
        public VaryCrossSlopeList()
        {
        }

        public VaryCrossSlopeList(IEnumerable<VaryingCrossSlope> collection) : base(collection)
        {
        }
        internal void Swap(VaryCrossSlopeList list, int indexA, int indexB)
        {
            VaryingCrossSlope tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }
    }
}
