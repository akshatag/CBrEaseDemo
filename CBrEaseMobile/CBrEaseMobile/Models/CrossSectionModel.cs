﻿using System.Collections.Generic;

namespace CBrEaseMobile.Models
{
    public class CrossSectionModel
    {
        public List<CrossSections> CrossSections { get; set; }
    }

    public class CrossSections
    {
        public int Number { get; set; }
        public string BaseDate { get; set; }
        public string BaseDateSecondary { get; set; }
        public string TypeString { get; set; }
        public int Type { get; set; }
        public string Comments { get; set; }
        public string CollectedBy { get; set; }
        public double VerticalDistance { get; set; }
        public double VerticalAdjustment { get; set; }
        public bool HasReferenceFace { get; set; }
        public bool HasConstantElevation { get; set; }
        public double Elevation { get; set; }
        public List<CrossSectionPoints> Points { get; set; } = new List<CrossSectionPoints>();
    }

    public class CrossSectionPoints
    {
        public int Id { get; set; }
        public int From { get; set; }
        public double HorizontalDist { get; set; }
        public double VerticalDist { get; set; }
        public bool IsVerticalAdjEnabled { get; set; }
        public string AddVerticalAdj { get; set; }
        public string Description { get; set; }
        public string StationString { get; set; }
        public double Station { get; set; }
        public string ElevationString { get; set; }
        public double Elevation { get; set; }
    }

}
