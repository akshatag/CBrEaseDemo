﻿using System.Collections.Generic;

namespace CBrEaseMobile.Models
{
    public class ApplicationData
    {
        public string CbzVersion { get; set; }

        public ProjectInfoModel ProjectInfo { get; set; }

        public VerticalAlignment Alignment { get; set; }
        public LevelGrade LGrade { get; set; }
        public ConstantGrade CGrade { get; set; }
        public VerticalCurveList VertCurveList { get; set; }

        public string CrossSlopeType;
        public double ConstCrossSlope;
        public VaryCrossSlopeList VCSList { get; set; }
        public double CrossSlopeHorizDist { get; set; }

        public Superstructure BridgeSuperstructure { get; set; }
        public VSDList VSDList { get; set; }

        public List<SubstructureItem> SubstructureInfo { get; set; }

        public CrossSectionModel CrossSection { get; set; }

        public StationDepthList StationDepthList { get; set; }

        public string NumberOfSavedCharts { get; set; }
        public string SavedChartData { get; set; }
    }
}
