﻿using System;
using CBrEaseMobile.Models;
using CBrEaseMobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CBrEaseMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CrossSectionsView : BaseView
    {
        public CrossSectionsView()
        {
            InitializeComponent();
            if (Device.RuntimePlatform == Device.Android)
            {
                CommentVB.OnTextChanged += s => RecognizeVoice(s, 1);
                CollectedByVB.OnTextChanged += s => RecognizeVoice(s, 2);
                VertDistVB.OnTextChanged += s => RecognizeVoice(s, 3);
                VertAdjVB.OnTextChanged += s => RecognizeVoice(s, 4);

                VerticalDistEntry.Keyboard = Keyboard.Numeric;
                HorDistEntry.Keyboard = Keyboard.Numeric;
            }
        }

        private void RecognizeVoice(string input, int fieldValue)
        {
            if (BindingContext is CrossSectionsViewModel vm)
            {
                vm.ActiveVoiceField = fieldValue;
                vm.ShowInputVoice(input);
            }
        }

        private void CrossSectionDate_OnTapped(object sender, EventArgs e)
        {
            var vm = BindingContext as CrossSectionsViewModel;
            if (vm.IsPastXSections)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ExistingDatePicker.Focus();
                });
            }
        }

        private void Type_OnTapped(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                TypePicker.Focus();
            });
        }

        //private void Horizontal_OnUnfocused(object sender, FocusEventArgs e)
        //{
        //    var element = (Entry)sender;
        //    var id = element.ClassId;
        //    var vm = BindingContext as CrossSectionsViewModel;
        //    vm.EditXSectionPoints("HorDist", Convert.ToInt32(id), element.Text);
        //}

        //private void Vertical_OnUnfocused(object sender, FocusEventArgs e)
        //{
        //    var element = (Entry)sender;
        //    var id = element.ClassId;
        //    var vm = BindingContext as CrossSectionsViewModel;
        //    vm.EditXSectionPoints("VertDist", Convert.ToInt32(id), element.Text);
        //}

        //private void VertAdj_OnCheckedChanged(object sender, EventArgs<bool> e)
        //{
        //    var element = (CheckBox)sender;
        //    var id = element.ClassId;
        //    var vm = BindingContext as CrossSectionsViewModel;
        //    vm.EditXSectionPoints("VertAdj", Convert.ToInt32(id), element.Checked);
        //}

        private void ListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;

            string response = string.Empty;
            var item = (CrossSectionPoints)e.SelectedItem;
            var vm = BindingContext as CrossSectionsViewModel;
            Device.BeginInvokeOnMainThread(async () =>
            {
                response = await DisplayActionSheet("Do you want to?", "Cancel", null, "Delete", "Edit");
                switch (response)
                {
                    case "Edit":
                        vm.EditThisPoint(item.Id);
                        break;

                    case "Delete":
                        vm.DeleteThisPoint(item.Id);
                        break;

                    default:
                        break;
                }

            ((ListView)sender).SelectedItem = null;
            });
        }
    }
}