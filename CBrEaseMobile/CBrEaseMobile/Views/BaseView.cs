﻿using CBrEaseMobile.ViewModels;
using Xamarin.Forms;

namespace CBrEaseMobile.Views
{
    public class BaseView : ContentPage
    {
        public BaseView()
        {

        }

        protected override bool OnBackButtonPressed()
        {
            var page = BindingContext as ViewModelBase;
            return page != null && page.OnHardwareBackButtonPressed();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            NavigationPage.SetHasBackButton(this, false);
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
