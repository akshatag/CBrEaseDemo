﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CBrEaseMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeView : StackLayout
    {
        public HomeView()
        {
            InitializeComponent();
            var selectedTab = Application.Current.Properties.ContainsKey("SelectedTab") ? Application.Current.Properties["SelectedTab"].ToString() : "1";
            TabsToDefault();
            switch (selectedTab)
            {
                case "1":
                    SummaryStack.BackgroundColor = Color.FromHex("#0E538E");
                    SummaryLabel.TextColor = Color.White;
                    break;
                case "2":
                    ChartStack.BackgroundColor = Color.FromHex("#0E538E");
                    ChartLabel.TextColor = Color.White;
                    break;
                case "3":
                    XSectionStack.BackgroundColor = Color.FromHex("#0E538E");
                    XSectionLabel.TextColor = Color.White;
                    break;
                default:
                    break;
            }
        }

        private void TabsToDefault()
        {
            SummaryStack.BackgroundColor = Color.White;
            ChartStack.BackgroundColor = Color.White;
            XSectionStack.BackgroundColor = Color.White;
            SummaryLabel.TextColor = Color.FromHex("#0E538E");
            ChartLabel.TextColor = Color.FromHex("#0E538E");
            XSectionLabel.TextColor = Color.FromHex("#0E538E");
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            Margin = new Thickness(0);
            if (Device.RuntimePlatform == Device.iOS)
            {
                //if (height > width)
                Margin = new Thickness(0, 20, 0, 0);
            }
            base.OnSizeAllocated(width, height);
        }
    }
}