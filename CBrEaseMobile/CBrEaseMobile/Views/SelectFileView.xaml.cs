﻿using CBrEaseMobile.ViewModels;
using Xamarin.Forms.Xaml;

namespace CBrEaseMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectFileView : BaseView
    {
        public SelectFileView()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            var vm = BindingContext as SelectFileViewModel;
            vm.InvokeLicense();
            base.OnAppearing();
        }
    }
}