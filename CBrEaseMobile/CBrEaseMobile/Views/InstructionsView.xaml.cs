﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace CBrEaseMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InstructionsView : PopupPage
    {
        public InstructionsView()
        {
            InitializeComponent();
        }
    }
}