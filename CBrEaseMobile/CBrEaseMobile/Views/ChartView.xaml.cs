﻿using CBrEaseMobile.ViewModels;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs;
using XLabs.Forms.Controls;

namespace CBrEaseMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChartView : BaseView
    {
        private PanGestureRecognizer pan;

        public ChartView()
        {
            InitializeComponent();
            pan = new PanGestureRecognizer();
            pan.PanUpdated += OnPanUpdated;
        }

        private bool IsInvokedByCheckbox;

        private void DataCanvas_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            SKImageInfo dataInfo = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas dataCanvas = surface.Canvas;
            dataCanvas.Clear();
            var vm = BindingContext as ChartViewModel;
            vm.DataCanvas = dataCanvas;
            vm.DataInfo = dataInfo;

            vm.DrawChart();
            vm.DrawBridge();
            vm.DrawCrossSections();

            if (!IsInvokedByCheckbox)
            {
                LegendsStack.Children.Clear();
                var number = vm?.XSectionList.Count;
                for (int i = 0; i < number; i++)
                {
                    var checkbox = new CheckBox();
                    checkbox.DefaultText = vm.XSectionList[i].Name;
                    checkbox.TextColor = vm.XSectionList[i].Color.ToFormsColor();
                    if (Device.RuntimePlatform == Device.iOS)
                        checkbox.WidthRequest = 130;
                    checkbox.Checked = vm.XSectionList[i].Visible;
                    checkbox.AutomationId = i.ToString();
                    checkbox.CheckedChanged += OnCheckChanged;
                    LegendsStack.Children.Add(checkbox);
                }
            }
        }

        private void OnCheckChanged(object sender, EventArgs<bool> e)
        {
            try
            {
                IsInvokedByCheckbox = true;
                var checkbox = (CheckBox)sender;
                var id = Convert.ToInt32(checkbox.AutomationId);
                var vm = BindingContext as ChartViewModel;
                vm.XSectionList[id].Visible = e.Value;
                DataCanvas.InvalidateSurface();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            NewXSectionBtn.IsVisible = width < height;
            base.OnSizeAllocated(width, height);
        }

        #region ZOOMING AND PANNING

        private double StartScale;
        private double CurrentScale;
        private double xOffset;
        private double yOffset;
        private double MIN_SCALE;
        private double MAX_SCALE;

        private void PinchGestureRecognizer_OnPinchUpdated(object sender, PinchGestureUpdatedEventArgs e)
        {
            if (e.Status == GestureStatus.Started)
            {

                //Disable Swipe of the carouselview or view pager here
                // Store the current scale factor applied to the wrapped user interface element,
                // and zero the components for the center point of the translate transform.
                StartScale = DataCanvas.Scale;
                DataCanvas.AnchorX = 0;
                DataCanvas.AnchorY = 0;
            }
            if (e.Status == GestureStatus.Running)
            {
                // Calculate the scale factor to be applied.
                CurrentScale += (e.Scale - 1) * StartScale;
                CurrentScale = Math.Max(1, CurrentScale);

                // The ScaleOrigin is in relative coordinates to the wrapped user interface element,
                // so get the X pixel coordinate.
                double renderedX = DataCanvas.X + xOffset;
                double deltaX = renderedX / Width;
                double deltaWidth = Width / (DataCanvas.Width * StartScale);
                double originX = (e.ScaleOrigin.X - deltaX) * deltaWidth;

                // The ScaleOrigin is in relative coordinates to the wrapped user interface element,
                // so get the Y pixel coordinate.
                double renderedY = DataCanvas.Y + yOffset;
                double deltaY = renderedY / Height;
                double deltaHeight = Height / (DataCanvas.Height * StartScale);
                double originY = (e.ScaleOrigin.Y - deltaY) * deltaHeight;

                // Calculate the transformed element pixel coordinates.
                double targetX = xOffset - (originX * DataCanvas.Width) * (CurrentScale - StartScale);
                double targetY = yOffset - (originY * DataCanvas.Height) * (CurrentScale - StartScale);

                // Apply translation based on the change in origin.
                DataCanvas.TranslationX = targetX.Clamp(-DataCanvas.Width * (CurrentScale - 1), 0);
                DataCanvas.TranslationY = targetY.Clamp(-DataCanvas.Height * (CurrentScale - 1), 0);

                // Apply scale factor.
                DataCanvas.Scale = CurrentScale;
            }
            if (e.Status == GestureStatus.Completed)
            {
                // Store the translation delta's of the wrapped user interface element.
                xOffset = DataCanvas.TranslationX;
                yOffset = DataCanvas.TranslationY;
                if (!DataCanvas.GestureRecognizers.Contains(pan))
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DataCanvas.GestureRecognizers.Add(pan);

                    });
                }
                if (DataCanvas.Scale == MIN_SCALE)
                {
                    if (DataCanvas.GestureRecognizers.Contains(pan))
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DataCanvas.GestureRecognizers.Remove(pan);

                        });
                    }
                    //Enable Swipe of the carouselview or view pager here
                }
            }
        }

        private double StartX;
        private double StartY;
        private double x;
        private double y;

        void OnPanUpdated(object sender, PanUpdatedEventArgs e)
        {
            switch (e.StatusType)
            {
                case GestureStatus.Started:
                    StartX = (1 - x) * Width;
                    StartY = (1 - y) * Height;
                    break;
                case GestureStatus.Running:
                    x = Clamp(1 - (StartX + e.TotalX) / Width, 0, 1);
                    y = Clamp(1 - (StartY + e.TotalY) / Height, 0, 1);

                    DataCanvas.AnchorX = x;
                    DataCanvas.AnchorY = y;

                    DataCanvas.TranslationX = DataCanvas.AnchorX;
                    DataCanvas.TranslationY = DataCanvas.AnchorY;
                    break;
            }
        }

        private void TapGestureRecognizer_OnTapped(object sender, EventArgs e)
        {
            CurrentScale = 1;
            DataCanvas.Scale = CurrentScale;
            DataCanvas.TranslationX = 1;
            DataCanvas.TranslationY = 1;
        }

        private T Clamp<T>(T value, T minimum, T maximum) where T : IComparable
        {
            if (value.CompareTo(minimum) < 0)
                return minimum;
            else if (value.CompareTo(maximum) > 0)
                return maximum;
            else
                return value;
        }

        #endregion
    }
}
