﻿using CBrEaseMobile.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using Xamarin.Forms.Xaml;

namespace CBrEaseMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SavedFilesView : PopupPage
    {
        public SavedFilesView()
        {
            InitializeComponent();
        }

        private void ListView_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as Tuple<string, string, DateTime>;

            var vm = BindingContext as SavedFilesViewModel;

            vm.SetCbzFilePath(item.Item2);
        }
    }
}