﻿namespace CBrEaseMobile.Services
{
    internal class VoiceService
    {
        public static string ConvertToSingleDigit(string input)
        {
            var inputTrm = input?.Trim().ToLower();
            if (inputTrm == "one") return "1";
            else if (inputTrm == "two") return "2";
            else if (inputTrm == "three") return "3";
            else if (inputTrm == "four") return "4";
            else if (inputTrm == "five") return "5";
            else if (inputTrm == "six") return "6";
            else if (inputTrm == "seven") return "7";
            else if (inputTrm == "eight") return "8";
            else if (inputTrm == "nine") return "9";
            else return input;
        }
    }
}
