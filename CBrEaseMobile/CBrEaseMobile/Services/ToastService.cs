﻿using Xamarin.Forms;

namespace CBrEaseMobile.Services
{
    internal class ToastService
    {
        public static void ShowErrorMessage(string message)
        {
            DependencyService.Get<IToastService>().ShowShortAlert(message);
        }

        public static void ShowSuccessMessage(string message)
        {
            DependencyService.Get<IToastService>().ShowShortAlert(message);
        }

        public static void ShowLongToast(string message)
        {
            DependencyService.Get<IToastService>().ShowLongAlert(message);
        }
    }
}
