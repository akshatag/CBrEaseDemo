﻿using CBrEaseMobile.FilePicker;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CBrEaseMobile.Services
{
    public interface IFileHandling
    {
        bool SaveLicense(string text);
        string GetLicense();
        bool SaveProjectFile(byte[] stream, string fileName, string filePath);
        List<Tuple<string, string, DateTime>> GetSavedFiles();
        Stream GetFileStream(string path);
    }

    public interface IToastService
    {
        void ShowShortAlert(string message);
        void ShowLongAlert(string message);
    }

    public interface ISpeechToText
    {
        void Start();
        void Stop();
        event EventHandler<EventArgsVoiceRecognition> textChanged;
    }

    public interface IFilePicker
    {
        Task<FileData> PickFile();
    }

    public interface IApplication
    {
        void Exit();
    }
}