﻿using System;

namespace CBrEaseMobile.Services
{
    public class EventArgsVoiceRecognition : EventArgs
    {
        public EventArgsVoiceRecognition(string text, bool isFinal)
        {
            Text = text;
            IsFinal = isFinal;
        }
        public string Text { get; set; }
        public bool IsFinal { get; set; }
    }
}
