﻿using CBrEaseMobile.Models;
using System;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;

namespace CBrEaseMobile.Services
{
    public class FileReadWrite
    {
        public static void WriteInCbzFile(ApplicationData appData, string fileName, bool isAutoSave, string filePath = "")
        {
            var BreaseFile = new MemoryStream();

            try
            {
                using (StreamWriter f = new StreamWriter(BreaseFile))
                {
                    f.WriteLine("Version=" + appData.CbzVersion);
                    WriteProjectInfo(f, appData.ProjectInfo);
                    WriteVerticalAlignment(f, appData.Alignment, appData.LGrade, appData.CGrade, appData.VertCurveList);
                    WriteCrossSlope(f, appData.CrossSlopeType, appData.ConstCrossSlope, appData.VCSList, appData.CrossSlopeHorizDist);
                    WriteSuperstructure(f, appData.BridgeSuperstructure, appData.VSDList);
                    WriteSubstructure(f, appData.SubstructureInfo);
                    WriteCrossSection(f, appData.CrossSection?.CrossSections);
                    WriteSavedCharts(f, appData.NumberOfSavedCharts, appData.SavedChartData);
                }
                if (DependencyService.Get<IFileHandling>().SaveProjectFile(BreaseFile.ToArray(), fileName, filePath))
                {
                    if (Device.RuntimePlatform == Device.Android)
                    {
                        if (isAutoSave)
                            ToastService.ShowLongToast("Changes have been made to the existing 'cbz' file.");
                        else
                            ToastService.ShowLongToast("File successfully saved to 'CBrEase' folder in root storage.");
                    }
                    else if (Device.RuntimePlatform == Device.iOS)
                    {
                        ToastService.ShowLongToast("Changes have been saved. You can look for '" + fileName + "' in the documents through iTunes.");
                    }
                }
                else
                {
                    ToastService.ShowErrorMessage("An error occured while saving the file to the system.");
                }
            }
            catch
            {
                ToastService.ShowErrorMessage("An error occured while writing data to the file.");
            }
        }

        public static void WriteProjectInfo(StreamWriter sw, ProjectInfoModel ProjectInfo)
        {
            sw.WriteLine("#PROJECT INFORMATION#");
            sw.WriteLine("Bridge Number=" + ProjectInfo.BridgeId);
            sw.WriteLine("Bridge Name=" + ProjectInfo.BridgeName);
            sw.WriteLine("Location=" + ProjectInfo.BridgeLocation);
            sw.WriteLine("X-Section Side=" + ProjectInfo.XSectionSide);
            sw.WriteLine("Unit System=" + ProjectInfo.UnitSystem);
            sw.WriteLine("Original User=" + ProjectInfo.OriginalUser);
            sw.WriteLine("Current User=" + ProjectInfo.CurrentUser);

        }

        private static void WriteVerticalAlignment(StreamWriter sw, VerticalAlignment Alignment, LevelGrade LGrade, ConstantGrade CGrade, VerticalCurveList VertCurveList)
        {
            sw.WriteLine("");
            sw.WriteLine("#VERTICAL ALIGNMENT DATA#");
            sw.WriteLine("Vertical Alignment Type=" + Alignment.TypeOfAlignment);
            switch (Alignment.TypeOfAlignment)
            {
                case 1:
                    sw.WriteLine("Elevation=" + LGrade.Elevation);
                    break;
                case 2:
                    sw.WriteLine("Station=" + CGrade.Station);
                    sw.WriteLine("Elevation=" + CGrade.Elevation);
                    sw.WriteLine("Grade=" + CGrade.Grade);
                    break;
                case 3:
                    var numVCs = VertCurveList.Count;

                    sw.WriteLine("Number of Vertical Curves=" + numVCs);
                    sw.WriteLine("Curve No. , BVC Sta, BVC El, BVC Grade, EVC Sta, EVC El, EVC Grade");
                    for (var i = 0; i < numVCs; i++)
                    {
                        var vcVals = VertCurveList[i].Number.ToString();
                        vcVals = vcVals + "," + VertCurveList[i].BvcStation.ToString();
                        vcVals = vcVals + "," + VertCurveList[i].BvcElevation.ToString();
                        vcVals = vcVals + "," + (VertCurveList[i].BvcGrade / 100).ToString();
                        vcVals = vcVals + "," + VertCurveList[i].EvcStation.ToString();
                        vcVals = vcVals + "," + VertCurveList[i].EvcElevation.ToString();
                        vcVals = vcVals + "," + (VertCurveList[i].EvcGrade / 100).ToString();

                        sw.WriteLine(vcVals);
                    }
                    break;
            }
        }

        private static void WriteCrossSlope(StreamWriter sw, string CrossSlopeType, double ConstCrossSlope, VaryCrossSlopeList VCSList, double CrossSlopeHorizDist)
        {
            sw.WriteLine("");
            sw.WriteLine("#CROSS SLOPE DATA#");
            sw.WriteLine("Cross Slope Horizontal Distance=" + CrossSlopeHorizDist);
            sw.WriteLine("Cross Slope Type=" + CrossSlopeType);

            switch (CrossSlopeType)
            {
                case "Constant":
                    sw.WriteLine("Constant Cross Slope=" + ConstCrossSlope);
                    break;
                case "Varying Cross Slope":
                    var numCsPts = VCSList.Count;
                    sw.WriteLine("Number of Cross Slope Pts=" + numCsPts);
                    sw.WriteLine("Sta, Slope");
                    for (var i = 0; i < numCsPts; i++)
                        sw.WriteLine(FormatStationString(VCSList[i].Station.ToString()) + "," + Convert.ToDouble(VCSList[i].Slope).ToString());
                    break;
            }
        }

        public static string FormatStationString(string sta)
        {
            if (sta.Contains("+") || sta == "") return sta;
            var formattedStation = (Math.Truncate(Convert.ToDouble(sta) / 100)) + "+" +
                                      (Convert.ToDouble(sta) % 100).ToString("00.00");
            return formattedStation;
        }

        private static void WriteSuperstructure(StreamWriter sw, Superstructure BridgeSuperstructure, VSDList VSDList)
        {
            sw.WriteLine("");
            sw.WriteLine("#SUPERSTRUCTURE DATA#");
            sw.WriteLine("Constant Structural Depth=" + BridgeSuperstructure.Thickness);
            sw.WriteLine("Number of Variable Structural Depth Templates=" + VSDList.Count);

            foreach (var vsd in VSDList)
            {
                sw.WriteLine("VSD Template Name=" + vsd.NameOfVsd);
                sw.WriteLine("Number of Key Points=" + vsd.Count);
                sw.WriteLine("Distance, Depth");
                foreach (var keyPt in vsd)
                    sw.WriteLine(keyPt.Distance + "," + keyPt.Depth);
            }
        }

        private static void WriteSubstructure(StreamWriter sw, List<SubstructureItem> SubItemsList)
        {
            sw.WriteLine("");
            sw.WriteLine("#SUBSTRUCTURE DATA#");
            sw.WriteLine("Number of Substructure Items=" + SubItemsList.Count);
            sw.WriteLine("");

            foreach (var item in SubItemsList)
            {
                sw.WriteLine("Item Number=" + item.ItemNumber);
                sw.WriteLine("Name Type=" + item.Name);
                sw.WriteLine("Station=" + item.StationString);
                sw.WriteLine("Bridge Skew=" + item.BridgeSkew);
                sw.WriteLine("Variable Structural Depth Left=" + item.VsdLeft);
                sw.WriteLine("Variable Structural Depth Right=" + item.VsdRight);
                sw.WriteLine("Foundation Type=" + item.FoundationType);
                sw.WriteLine("\tColumn Information");
                sw.WriteLine("\t\tTop Width, Bottom Width, Top El.");
                sw.WriteLine("\t\t" + item.ColumnTopWidth + "," + item.ColumnBottomWidth + "," + item.ColumnTopElevation);
                sw.WriteLine("\tFooting Information");
                sw.WriteLine("\t\tWidth, Thickness, TOF El., BOF El., BOTremie El.");
                sw.WriteLine("\t\t" + item.FootingWidth + "," + item.FootingThickness + "," + item.FootingTopElevation + "," + item.FootingBottomElevation + "," + item.TremieSealBottomElevation);
                sw.WriteLine("\tPile Information");
                sw.WriteLine("\t\tWidth, PTE, Number, Spacing");
                sw.WriteLine("\t\t" + item.PileWidth + "," + item.PileTipElevation + "," + item.PileNumber + "," + item.PileSpacing);
            }
        }

        private static void WriteCrossSection(StreamWriter sw, List<CrossSections> XList)
        {
            sw.WriteLine("");
            sw.WriteLine("#CROSS SECTION DATA#");
            sw.WriteLine("Number of Cross Section Items=" + XList.Count);
            sw.WriteLine("");
            foreach (var xs in XList)
            {
                sw.WriteLine("Cross Section Date=" + xs.BaseDate);
                var sectionType = string.Empty;
                switch (xs.Type)
                {
                    case 0:
                        sectionType = "Channel X-Section";
                        break;
                    case 1:
                        sectionType = "Engineering Plans";
                        break;
                    case 2:
                        sectionType = "Engineering Survey";
                        break;
                    default: break;
                }
                sw.WriteLine("Section Type=" + sectionType);
                sw.WriteLine("Comments=" + xs.Comments);
                sw.WriteLine("Collector=" + xs.CollectedBy);
                sw.WriteLine("Vertical Offset=" + xs.VerticalDistance);
                sw.WriteLine("Vertical Adjustment=" + xs.VerticalAdjustment);
                sw.WriteLine("Ref Face=" + xs.HasReferenceFace);
                sw.WriteLine("Ref Constant Elevation=" + xs.HasConstantElevation);
                if (xs.HasConstantElevation) sw.WriteLine("Constant Elevation=" + xs.Elevation);
                sw.WriteLine();
                sw.WriteLine("Number of Points=" + xs.Points.Count);
                sw.WriteLine("\tPoint Number, From Item, Horiz. Dist., Vert. Dist., Add Adjust., Description, Station, Elev.");

                foreach (var pt in xs.Points)
                {
                    var Desc = @"""" + pt.Description + @"""";

                    sw.WriteLine("\t" + pt.Id + "," + pt.From + "," + pt.HorizontalDist + "," + pt.VerticalDist + "," +
                                 pt.IsVerticalAdjEnabled + "," + Desc + "," + pt.Station + "," + pt.ElevationString);
                }
                sw.WriteLine("");
            }
        }

        public static void WriteSavedCharts(StreamWriter sw, string NumberOfSavedCharts, string SavedChartData)
        {
            sw.WriteLine("");
            sw.WriteLine("#CHART DATA#");
            sw.WriteLine("Number of Saved Charts=" + NumberOfSavedCharts);
            sw.WriteLine("");
            sw.Write(SavedChartData);
        }
    }
}
