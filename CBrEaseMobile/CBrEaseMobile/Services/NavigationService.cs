﻿using CBrEaseMobile.Utilities;
using CBrEaseMobile.ViewModels;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CBrEaseMobile.Services
{
    internal class NavigationService
    {
        public static INavigation Navigation { get; set; }
        protected PageLocator PageLocator { get; set; }


        protected App App { get; set; }

        public NavigationService()
        {
            this.PageLocator = new PageLocator();
            App = Application.Current as App;
        }

        public void SetMainViewModel<T>(object args = null, string barColor = null) where T : ViewModelBase
        {
            Action setmainView = () =>
            {
                try
                {
                    var page = ResolvePageFor<T>(args);

                    if (page == null)
                        throw new Exception("Resolve page for " + typeof(T).Name + " returned null!");

                    var masterDetailPage = page as MasterDetailPage;

                    if (masterDetailPage != null)
                    {
                        Navigation = masterDetailPage.Detail.Navigation;
                        App.MainPage = masterDetailPage;
                    }
                    else
                    {
                        var navigationPage = new NavigationPage(page);
                        Navigation = navigationPage.Navigation;
                        var masterPage = App.MainPage as MasterDetailPage;
                        if (masterPage != null)
                            masterPage.Detail = navigationPage;
                        else
                            App.MainPage = navigationPage;
                    }
                }
                catch (Exception ex)
                {

                }
            };

            if (Device.RuntimePlatform == Device.Android)
            {
                Device.BeginInvokeOnMainThread(setmainView);
            }
            else
            {
                if (App.MainPage == null)
                {
                    setmainView();
                }
                else
                {
                    Device.BeginInvokeOnMainThread(setmainView);
                }
            }
            CloseDrawerMenu();
        }

        public Task NavigateToAsync<T>(object args = null) where T : ViewModelBase
        {
            var page = ResolvePageFor<T>(args);
            if (page == null)
            {
                return Task.FromResult(false);
            }
            var tcs = new TaskCompletionSource<bool>();

            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    await Navigation.PushAsync(page);
                    tcs.SetResult(true);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            });

            return tcs.Task;
        }


        public void NavigateToModal<T>(object args = null) where T : ViewModelBase
        {
            var page = ResolvePageFor<T>(args);

            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    await Navigation.PushModalAsync(page);
                }
                catch (Exception ex)
                {

                }
            });
        }

        public void NavigateToPopup<T>(object args = null) where T : ViewModelBase
        {
            var page = ResolvePageFor<T>(args);

            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    await PopupNavigation.Instance.PushAsync(page as PopupPage);
                }
                catch (Exception ex)
                {

                }
            });
        }

        public bool IsPopping
        {
            get;
            set;
        }

        public Task PopAsync()
        {
            return Navigation.PopAsync();
        }

        public Task PopModalAsync()
        {
            return Navigation.PopModalAsync();
        }

        public Task PopToRootAsync()
        {
            return Navigation.PopToRootAsync();
        }

        public Page ResolvePageFor<T>(object args = null) where T : ViewModelBase
        {
            var page = PageLocator.ResolvePageAndViewModel(typeof(T), args);
            return page;
        }

        public void RemoveFromNavigationStack<T>(bool removeFirstOccurenceOnly = true) where T : ViewModelBase
        {
            if (Navigation != null)
            {
                Type pageType = PageLocator.ResolvePageType(typeof(T));

                var navigationStack = Navigation.NavigationStack.Reverse();
                foreach (var page in navigationStack)
                {
                    if (page.GetType() == pageType)
                    {
                        Navigation.RemovePage(page);
                        if (removeFirstOccurenceOnly)
                            break;
                    }
                }
            }
        }

        public IReadOnlyList<ViewModelBase> GetNavigationStack()
        {
            try
            {
                if (Navigation != null && Navigation.NavigationStack != null && Navigation.NavigationStack.Any())
                {
                    return Navigation.NavigationStack
                        .Where(page => page?.BindingContext is ViewModelBase)
                        .Select(page => page.BindingContext as ViewModelBase).ToList();
                }
            }
            catch (Exception ex)
            {

            }

            return Enumerable.Empty<ViewModelBase>().ToList();
        }

        public bool IsRootPage
        {
            get { return Navigation.NavigationStack.Count == 1; }
        }

        public ViewModelBase CurrentViewModel
        {
            get { return CurrentPage?.BindingContext as ViewModelBase; }
        }

        public ViewModelBase CurrentModalViewModel
        {
            get { return Navigation.ModalStack.LastOrDefault()?.BindingContext as ViewModelBase; }
        }

        public Page CurrentPage
        {
            get { return Navigation?.NavigationStack?.LastOrDefault(); }
        }

        public void OpenDrawerMenu()
        {
            PresentDrawerMenu(true);
        }

        public void CloseDrawerMenu()
        {
            PresentDrawerMenu(false);
        }

        public void ToggleDrawerMenu()
        {
            var masterDetailPage = App.MainPage as MasterDetailPage;

            if (masterDetailPage != null)
                masterDetailPage.IsPresented = !masterDetailPage.IsPresented;
        }

        private void PresentDrawerMenu(bool isPresented)
        {
            var masterDetailPage = App.MainPage as MasterDetailPage;

            if (masterDetailPage != null)
                masterDetailPage.IsPresented = isPresented;
        }
    }
}
