﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CBrEaseMobile.Resources {
    using System;
    using System.Reflection;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CBrEaseMobile.Resources.Resource", typeof(Resource).GetTypeInfo().Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bridge Number / ID.
        /// </summary>
        internal static string BridgeId {
            get {
                return ResourceManager.GetString("BridgeId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bridge Location.
        /// </summary>
        internal static string BridgeLoc {
            get {
                return ResourceManager.GetString("BridgeLoc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bridge Name({0}).
        /// </summary>
        internal static string BridgeName {
            get {
                return ResourceManager.GetString("BridgeName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bridge {0}.
        /// </summary>
        internal static string BridgeTitle {
            get {
                return ResourceManager.GetString("BridgeTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CurrentUser.
        /// </summary>
        internal static string CurrUser {
            get {
                return ResourceManager.GetString("CurrUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Downstream.
        /// </summary>
        internal static string Downstream {
            get {
                return ResourceManager.GetString("Downstream", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Elevation ({0}).
        /// </summary>
        internal static string Elevation {
            get {
                return ResourceManager.GetString("Elevation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to English.
        /// </summary>
        internal static string Eng {
            get {
                return ResourceManager.GetString("Eng", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Horizontal Dist. ({0}).
        /// </summary>
        internal static string HorDist {
            get {
                return ResourceManager.GetString("HorDist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Metric.
        /// </summary>
        internal static string Metric {
            get {
                return ResourceManager.GetString("Metric", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Original User.
        /// </summary>
        internal static string OrgUser {
            get {
                return ResourceManager.GetString("OrgUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This app requires storage permissions to load and save files.
        ///Please allow the storage permission when prompted again.
        ///If you have permanently denied the permission, go to the application&apos;s &apos;Privacy Permissions&apos; and turn on necessary permissions..
        /// </summary>
        internal static string PermissionRequired {
            get {
                return ResourceManager.GetString("PermissionRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Project Information.
        /// </summary>
        internal static string ProjInfo {
            get {
                return ResourceManager.GetString("ProjInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Side of Project.
        /// </summary>
        internal static string SideProj {
            get {
                return ResourceManager.GetString("SideProj", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This page allows user to fill cross section point fields just using a single voice command.
        ///To use this feature tap the &apos;blue microphone&apos; icon. A popup shall appear that signifies that voice input is being taken.
        ///Speak specific keywords followed by field values.
        ///
        ///Keywords to be used for fields are defined and can be used as follows:
        ///From Item - &quot;from&quot;
        ///Vertical Distance - &quot;vertical&quot;
        ///Horizontal Distance - &quot;horizontal&quot;
        ///Add Vertical Adjustment - &quot;with offset&quot; if yes, &quot;no offset&quot; if no
        ///Comments/Descript [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string SingleVoiceInstruction {
            get {
                return ResourceManager.GetString("SingleVoiceInstruction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Station ({0}).
        /// </summary>
        internal static string Station {
            get {
                return ResourceManager.GetString("Station", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trial period is over. No new data could be saved..
        /// </summary>
        internal static string TrialEndText {
            get {
                return ResourceManager.GetString("TrialEndText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Trial days remaining: {0}.
        /// </summary>
        internal static string TrialTimeText {
            get {
                return ResourceManager.GetString("TrialTimeText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unit System.
        /// </summary>
        internal static string UnitSys {
            get {
                return ResourceManager.GetString("UnitSys", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upstream.
        /// </summary>
        internal static string Upstream {
            get {
                return ResourceManager.GetString("Upstream", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Info.
        /// </summary>
        internal static string UserInfo {
            get {
                return ResourceManager.GetString("UserInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vertical Dist. ({0}).
        /// </summary>
        internal static string VertDist {
            get {
                return ResourceManager.GetString("VertDist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vertical Measurement Adjustment ({0}) (e.g., Tape Leader, Rod Height).
        /// </summary>
        internal static string VerticalAdjLabel {
            get {
                return ResourceManager.GetString("VerticalAdjLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vert. Dist. from Deck to Measuring Pt.({0}) (&quot;+&quot; for Rail and &quot;-&quot; for Soffit).
        /// </summary>
        internal static string VerticalDistLabel {
            get {
                return ResourceManager.GetString("VerticalDistLabel", resourceCulture);
            }
        }
    }
}
