﻿using CBrEaseMobile.Services;
using ToastIOS;

[assembly: Xamarin.Forms.Dependency(typeof(CBrEaseMobile.iOS.DependencyServices.ToastService))]
namespace CBrEaseMobile.iOS.DependencyServices
{
    public class ToastService : IToastService
    {
        public void ShowShortAlert(string message)
        {
            Toast.MakeText(message, Toast.LENGTH_SHORT).Show();
        }

        public void ShowLongAlert(string message)
        {
            Toast.MakeText(message, Toast.LENGTH_LONG).Show();
        }
    }
}