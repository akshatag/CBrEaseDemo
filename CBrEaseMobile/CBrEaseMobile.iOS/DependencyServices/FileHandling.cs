﻿using CBrEaseMobile.Services;
using Foundation;
using Security;
using System;
using System.Collections.Generic;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(CBrEaseMobile.iOS.DependencyServices.FileHandling))]
namespace CBrEaseMobile.iOS.DependencyServices
{
    public class FileHandling : IFileHandling
    {
        private readonly static string LicenseFileName = "License.lic";
        private readonly string Key = "cbrease_license";

        public bool SaveLicense(string licVal)
        {
            try
            {
                var record = new SecRecord(SecKind.GenericPassword)
                {
                    Generic = NSData.FromString(Key),
                    ValueData = NSData.FromString(licVal)
                };
                var result = SecKeyChain.Add(record);

                //var appPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                //var filePath = Path.Combine(appPath, LicenseFileName);
                //File.WriteAllText(filePath, text, Encoding.UTF8);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string GetLicense()
        {
            try
            {
                SecStatusCode result;
                var record = new SecRecord(SecKind.GenericPassword)
                {
                    Generic = NSData.FromString(Key)
                };
                var matchRecord = SecKeyChain.QueryAsRecord(record, out result);
                if (matchRecord != null)
                {
                    var value = matchRecord.ValueData.ToString();
                    return value;
                }

                return string.Empty;

                //var appPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                //var filePath = Path.Combine(appPath, LicenseFileName);
                //return File.ReadAllText(filePath);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        private readonly static string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        public bool SaveProjectFile(byte[] stream, string fileName, string filePath)
        {
            try
            {
                var brease = new MemoryStream(stream);
                filePath = Path.Combine(folderPath, fileName);
                using (var streamReader = new StreamReader(brease))
                {
                    string content = streamReader.ReadToEnd();
                    File.WriteAllText(filePath, content);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Tuple<string, string, DateTime>> GetSavedFiles()
        {
            var listFile = new List<Tuple<string, string, DateTime>>();

            if (Directory.Exists(folderPath))
            {
                var files = Directory.GetFiles(folderPath);

                foreach (var filePath in files)
                {
                    var name = Path.GetFileName(filePath);
                    var time = File.GetCreationTimeUtc(filePath);

                    if (name.IndexOf(".cbz") > 0)
                        listFile.Add(Tuple.Create(name, filePath, time));
                }
            }
            return listFile;
        }

        public Stream GetFileStream(string path)
        {
            var file = File.ReadAllBytes(path);

            return new MemoryStream(file);
        }
    }
}