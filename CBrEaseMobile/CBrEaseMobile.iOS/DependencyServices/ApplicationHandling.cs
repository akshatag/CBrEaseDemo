﻿using CBrEaseMobile.Services;
using System.Threading;

[assembly: Xamarin.Forms.Dependency(typeof(CBrEaseMobile.iOS.DependencyServices.ApplicationHandling))]
namespace CBrEaseMobile.iOS.DependencyServices
{
    public class ApplicationHandling : IApplication
    {
        public void Exit()
        {
            Thread.CurrentThread.Abort();
        }
    }
}