﻿using CBrEaseMobile.CustomControls;
using CBrEaseMobile.iOS.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(RoundEntry), typeof(RoundEntryRenderer))]
namespace CBrEaseMobile.iOS.CustomRenderers
{
    public class RoundEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.Background = null;
                Control.BorderStyle = UIKit.UITextBorderStyle.None;
            }
        }
    }
}